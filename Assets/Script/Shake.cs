﻿//----------------- Shake -----------------
//
//      概要 : 
//      内容 : 
//
//      作成者 : 今野
//
//      追加,変更 : 工藤
//


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour {

    private Vector3 _currentVec;            //現フレームのベクトル
    private Vector3 _previousVec;           //1つ前のフレームのベクトル
    private float _vecDot;                  //ベクトルの内積を求めたものを代入する
    private bool _shakeFlg = false;         //フィーバーかどうか

    //public AudioClip _tapSE;       //タップのSE

	// Use this for initialization
	void Start () {
        Audio.getInstance().loadSE((int)Const.AudioSE.SYAN,Const.AudioSEName.SYAN);
	}
	
	// Update is called once per frame
	void Update () 
    {
        if(_shakeFlg == true)
        {
            //1つ前のフレームのベクトルを代入する
            _previousVec = _currentVec;

            //現フレームのベクトルを代入
            _currentVec = Input.acceleration;

            //現フレームと1つ前のフレームの内積を計算
            _vecDot = Vector3.Dot(_currentVec, _previousVec);

            //内積を計算した際にベクトルが逆方向(90度～180度)に動いているときはスコアを加算する
            if (_vecDot < 0)
            {
                //GetComponent<AudioSource>().PlayOneShot(_tapSE);
                Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
                ScoreData.score += Const.AddScore.SHAKE;
                ScoreData.shakeCnt++;
            }
        }
        

	}

    
    public void setShakeFlg(bool flg)
    {
        _shakeFlg = flg;
    }

    public bool getShakeFlg()
    {
        return _shakeFlg;
    }

}
