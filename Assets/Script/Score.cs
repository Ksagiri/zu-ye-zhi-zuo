﻿//----------------- Scoreクラス -----------------
//
//      概要 : スコアをつける
//      内容 : スコアをつけたり、PlaySceneのTextにスコアを反映させる
//
//      作成者 : 今野咲霧
//
//
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    private string[] _scoreName;            //スコアのスプライトのファイル名
    private Sprite[] _scoreSprite;          //スコアのスプライト
    private static int _scoreRankId;        //スコアランクのID(リザルトシーンで持っていく値)
    private int _maxNotes;                  //Maxノーツ数
    private NumberFont _font;               //スプライトのフォントクラス
    private Life _life;                     //ライフ
    private static int _combo = 0;          //コンボ数
    private bool _fever = false;            //フィーバータイムかどうかのフラグ

    public Image[] _comboNumTex;            //コンボ数のテクスチャ
    public Image[] _scoreNumTex;            //スコア数のテクスチャ
    public Image _score;                    //スコアの名前を表示
    public Image _comboTex;                 //コンボテクスチャ

    //曲が終わったときのスコアに応じたランク
    enum ScoreRank
    {
        S,
        A,
        B,
        C
    }


    // Use this for initialization
    void Start()
    {
        ScoreData.score = 0;
        _combo = 0;

        _life = GameObject.Find("DataBase").GetComponent<Life>();

        //スコア用のスプライトを準備
        _scoreSprite = new Sprite[5];
        _scoreName = new string[5] { "PERFECT", "GREAT", "GOOD", "BAD", "MISS" };
        for(int i = 0; i < _scoreSprite.Length; i++)
        {
            _scoreSprite[i] = Resources.Load<Sprite>("Score/" + _scoreName[i]);
        }

        //スコアのスプライトを非表示にする
        _score.enabled = false;

        //コンボの数字の1桁目だけ残してあとは見えないようにする
        for (int i = 1; i < _comboNumTex.Length; i++)
        {
            _comboNumTex[i].enabled = false;
        }

        _font = GameObject.Find("DataBase").GetComponent<NumberFont>();



    }

    // Update is called once per frame
    void Update()
    {

        //スコア数を表示
        _font.showFont(ScoreData.score, _scoreNumTex);

        //フィーバータイムでなければコンボを表示する
        if(_fever == false)
        {
            //コンボ数を表示
            _font.showFontCombo(_combo, _comboNumTex);



            //コンボ数が0の場合はコンボ数を表示しているテクスチャを見えなくする
            if (_combo == 0)
            {
                _comboTex.enabled = false;

            }
            else
            {
                _comboTex.enabled = true;
            }
        }

    }

    //スコアを追加する
    //引数：ノーツのスコア
    //戻り値：なし
    public void addScore(int notesScore)
    {
        switch (notesScore)
        {
            case (int)Const.ScoreCate.PERFECT:
                calcScore(Const.AddScore.PAFECT, _life.getLife() > 0);
                setScoreSprite((int)Const.ScoreCate.PERFECT);
                ScoreData.perfectCnt++;
                if (ScoreData.maxComboCnt < _combo)
                {
                    ScoreData.maxComboCnt = _combo;
                }
                break;

            case (int)Const.ScoreCate.GREAT:
                //ライフがある場合
                calcScore(Const.AddScore.GREAT, _life.getLife() > 0);
                setScoreSprite((int)Const.ScoreCate.GREAT);
                ScoreData.greatCnt++;
                if (ScoreData.maxComboCnt < _combo)
                {
                    ScoreData.maxComboCnt = _combo;
                }
                break;

            case (int)Const.ScoreCate.GOOD:
                calcScore(Const.AddScore.GOOD, _life.getLife() > 0);
                setScoreSprite((int)Const.ScoreCate.GOOD);
                ScoreData.goodCnt++;
                if (ScoreData.maxComboCnt < _combo)
                {
                    ScoreData.maxComboCnt = _combo;
                }
                break;

            case (int)Const.ScoreCate.BAD:
                //ライフがある場合
                ScoreData.score += Const.AddScore.BAD;
                ScoreData.badCnt++;

                //ライフが0の場合
                if ((_life.getLife() > 0) == false)
                {
                    ScoreData.score = ScoreData.score + Const.AddScore.MISS / 2;
                }
                    
                //最大コンボ数とコンボ数を比較してコンボ数が大きければ
                if (ScoreData.maxComboCnt < _combo)
                {
                    ScoreData.maxComboCnt = _combo;
                }

                _combo = 0;
                setScoreSprite((int)Const.ScoreCate.BAD);

                break;
            case (int)Const.ScoreCate.MISS:
                ScoreData.missCnt++;
                if (ScoreData.maxComboCnt < _combo)
                {
                    ScoreData.maxComboCnt = _combo;
                }
                _combo = 0;

                setScoreSprite((int)Const.ScoreCate.MISS);
                break;
        }

        //0.8秒後スプライトの表示を非表示にする
        Invoke("setScoreEnabled", 0.8f);

        if (ScoreData.maxComboCnt < _combo)
        {
            ScoreData.maxComboCnt = _combo;
        }
    }

    //スコアを計算する
    //引数：constで設定したスコアの名前　ライフがゼロか
    //戻り値：なし
    public void calcScore(int scoreName,bool isLife)
    {
        //ライフが0の場合
        if (isLife == false)
        {
            ScoreData.score = ScoreData.score + (scoreName / 2);
            _combo++;
        }
        else
        {
            ScoreData.score = ScoreData.score + scoreName;
            _combo++;
        }

    }

    //スコアスプライトを消す
    //引数：なし
    //戻り値：なし
    private void setScoreEnabled()
    {
        _score.enabled = false;
    }

    public void setComboEnabled()
    {
        _fever = true;
        _comboTex.enabled = false;
        for(int i = 0; i < _comboNumTex.Length;i++)
        {
            _comboNumTex[i].enabled = false;
        }
    }

    //スプライトのスコアをセット
    //引数：スプライトの要素番号(スコアカテゴリー)
    //戻り値：なし
    private void setScoreSprite(int num)
    {
        _score.enabled = false;
        _score.enabled = true;
        _score.sprite = _scoreSprite[num];
    }

    //スコアランクIDを取得する
    //引数：なし
    //戻り値：スコアランクID
    public static int getScoreRankId()
    {
        return _scoreRankId;
    }

    //maxscoreをセットする
    //引数：曲のノーツ数
    //戻り値：なし
    public void setMaxScore(int notesSum)
    {
        //_maxScore = notesSum * PERFECT;
        _maxNotes = notesSum;// * Const.AddScore.PAFECT;
    }

    //ランクを決める
    //引数：なし
    //戻り値：なし
    public void calcScoreRank()
    {
        if (_maxNotes * Const.AddScore.PAFECT < ScoreData.score)
        {
            // S
            _scoreRankId = (int)ScoreRank.S;
        }
        else if (_maxNotes * Const.AddScore.GREAT < ScoreData.score)
        {
            // A
            _scoreRankId = (int)ScoreRank.A;
        }
        else if(_maxNotes * Const.AddScore.GOOD < ScoreData.score)
        {
            // B
            _scoreRankId = (int)ScoreRank.B;
        }
        else
        {
            // C
            _scoreRankId = (int)ScoreRank.C;
        }
    }

    //コンボ数を取得
    //引数：なし
    //戻り値：コンボ数
    public static int getCombo()
    {
        return _combo;
    }
    
}

