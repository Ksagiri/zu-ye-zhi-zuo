﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberFont : MonoBehaviour {
    private Sprite[] _numTex;

	// Use this for initialization
	void Awake () {

        //スライス前の全体が表示されている文字スプライトを取得
        Sprite[] sprites = Resources.LoadAll<Sprite>("Font/");
        _numTex = new Sprite[10];

        //スライスしたスプライトを取得
        for(int i = 0; i < _numTex.Length; i++)
        {
            _numTex[i] = System.Array.Find<Sprite>(sprites, (sprite) => sprite.name.Equals("数字フォント(仮)_" + i));
        }
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    //数字フォントを表示
    //引数：スコア　数字フォント
    //戻り値：なし
    public void showFont(int num, Image[] tex)
    {
        int digit = num;

        ////要素０番目には1桁目の値が格納される
        List<int> number = new List<int>();

        if(digit == 0)
        {
            number.Add(num);
        }

        //最後まできたら終了
        while( digit != 0)
        {
            num = digit % 10;
            digit = digit / 10;
            number.Add(num);
        }

        //数字フォントの切り替え
        for (int i = 0; i < number.Count; i++)
        {
            tex[i].sprite = _numTex[number[i]];
        }

    }

    //コンボ数を表示
    //引数：コンボ数　表示させる用の配列
    //戻り値：なし
    public void showFontCombo(int num, Image[] tex)
    {
        int digit = num;

        List<int> number = new List<int>();

        //桁数を確認
        while (digit != 0)
        {
            num = digit % 10;
            digit = digit / 10;
            number.Add(num);
        }

        //それぞれの桁に対応する数字を入れる
        for(int i = 0; i < number.Count; i++)
        {
            tex[i].sprite = _numTex[number[i]];
        }

        //コンボ数が3桁の場合
        if(number.Count >= 3)
        {
            tex[2].enabled = true;
            tex[1].enabled = true;
        }
        //コンボ数が2桁の場合
        else if(number.Count >= 2)
        {
            tex[1].enabled = true;
        }

        //ゼロコンボの場合
        //1の位・10の位・100の位の数字の表示を消す
        if(Score.getCombo() == 0)
        {
            tex[0].sprite = _numTex[0];
            tex[0].enabled = false;
            tex[1].enabled = false;
            tex[2].enabled = false;

        }
        else
        {
            //コンボが0以上の場合は1の位を表示させる
            tex[0].enabled = true;
        }
    }
}
