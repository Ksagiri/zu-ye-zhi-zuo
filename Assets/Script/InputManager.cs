﻿//----------------- InputManager -----------------
//
//      概要 : 入力を扱うクラス
//      内容 : タッチ判定を纏めている
//
//      作成者 : 工藤　祐平
//
//


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    private float _touchStartPosX;      //タッチされたX座標
    private float _touchEndPosX;        //タッチが終わったX座標

    private float _touchStartPosY;      //タッチされたY座標
    private float _touchEndPosY;        //タッチが終わったY座標

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        //常にタッチされたときのx座標を取得しておく
        if (Input.GetMouseButtonDown(0))
        {
            _touchStartPosX = Input.mousePosition.x;
            _touchStartPosY = Input.mousePosition.y;
        }
	}

    //機能 : タッチされたらタッチエフェクトを表示して画面がタッチされた意味でtrueを返す
    //引数 : なし
    //戻り値 : bool タッチされた/true タッチしていない/false
    public bool clickScreen()
    {
        if(Input.GetMouseButtonDown(0))
        {
            //タッチされた
            return true;
        }

        //タッチされていない
        return false;
        
    }

    //機能 : フリック判定をする(X座標)
    //          タッチされた位置と離した位置を比べて左右どちらにいったか確認する
    //引数 : なし
    //戻り値 : int型    左にフリック:FLICK_LEFT,右にフリック:FLICK_RIGHT,フリックしていない:0
    public int flickScreenX()
    {
        //指を離したときの座標を取得
        if(Input.GetMouseButtonUp(0))
        {
            _touchEndPosX = Input.mousePosition.x;


            //x座標の差を求める
            float defferrence = _touchEndPosX - _touchStartPosX;

            //後々y座標の値を追加して上限を設定する可能性有
            //差が正の値なら右フリック:FLICK_RIGHT(2)
            //負の値なら左フリック:FLICK_LEFT(1)
            if (defferrence > 0)
            {
                _touchStartPosX = 0;
                _touchEndPosX = 0;
                return (int)Const.TapCate.FLICK_RIGHT;
            }
            else if (defferrence < 0)
            {
                _touchStartPosX = 0;
                _touchEndPosX = 0;
                return (int)Const.TapCate.FLICK_LEFT;
            }
        }

        //フリックしていない
        return 0;
    }

    //フリック判定
    //引数：なし
    //戻り値：１(上)　２(下)
    public int flickScreenY()
    {
        if(Input.GetMouseButtonUp(0))
        {
            _touchEndPosY = Input.mousePosition.y;

            //y座標の差を求める
            float defferrence = _touchEndPosY - _touchStartPosY;

            if(defferrence > 0)
            {
                _touchStartPosY = 0;
                _touchEndPosY = 0;
                return 1;
            }
            else if(defferrence < 0)
            {
                _touchStartPosY = 0;
                _touchEndPosY = 0;
                return 2;
            }
        }
        return 0;
    }

    //機能 : タップされて長押し状態かどうか確認する
    //          タップされている/true   指が離れた/false
    //引数 : なし
    //戻り値 : bool タッチされている/true 指を離した/false
    public int longTap()
    {
        //画面にタッチした
        if (Input.GetMouseButtonDown(0))
        {
            //タッチされた
            return (int)Const.TapCate.LONG_START;
        }


        if (Input.GetMouseButtonUp(0))
        {
            //画面から指を離したら
            return (int)Const.TapCate.LONG_STOP;
        }

        return 0;
    }
}
