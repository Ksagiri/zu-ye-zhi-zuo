﻿//----------------- SelectScrollRectClothes -----------------
//
//      概要 : 着せ替え選択のスクロール管理
//              ScrollRectの拡張版なので、対象のScrollViewにつける
//      内容 : スクロール処理
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectScrollRectClothes : ScrollRect
{

    private float _pageHeight;                   //1ページ(ボタン)の幅

    private int _centerIndex = 0;        //前回の真ん中のボタン値,最上が0

    private SelectClothesButton _selectButton;    //着せ替えステージのクラス

    //初期
    protected override void Awake()
    {
        base.Awake();

        _selectButton = GameObject.Find("DataBase").GetComponent<SelectClothesButton>();

        initFunc();
    }

    public void initFunc()
    {
        GridLayoutGroup grid = content.GetComponent<GridLayoutGroup>();

        //前回選ばれた服IDを取得
        _centerIndex = SelectClothesButton.getClothesId();

        //1ページの幅(ボタンの縦幅)を取得
        _pageHeight = grid.cellSize.y + grid.spacing.y;

        //Contentのスクロール位置を決定する
        float destY = _centerIndex * _pageHeight;
        content.anchoredPosition = new Vector2(content.anchoredPosition.x, destY);

    }

    //ドラッグ開始
    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);

        //すべてのボタンを無効化する
        _selectButton.setInterctive(false);
    }

    //ドラッグ終了
    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

        //ドラッグ終了時にスクロールをとめる
        //スナップさせる位置が決まったあとに慣性を残さないため
        StopMovement();

        //スナップさせる位置を決める
        //スナップさせるボタンの値を決める
        int pageIndex = Mathf.RoundToInt(content.anchoredPosition.y / _pageHeight);

        //ページが変わっていないかつ、素早くドラッグした場合
        if (pageIndex == _centerIndex && Mathf.Abs(eventData.delta.y) >= 5)
        {
            pageIndex += (int)Mathf.Sign(eventData.delta.y);
        }

        //Contentのスクロール位置を決定する
        float destY = pageIndex * _pageHeight;
        content.anchoredPosition = new Vector2(content.anchoredPosition.x, destY);

        //ページが変わっていない判定を行うため前回スナップされていた位置を記憶しておく
        //ページ外に出た場合その値が保存されてしまうため対策としてありえないページは保存しない
        if (pageIndex < 0)
            pageIndex = 0;
        else if (pageIndex >= Const.Consts.CLOTHES)
            pageIndex = Const.Consts.CLOTHES - 1;

        //中央のボタンのみ有効化
        _selectButton.centerButtonActive(pageIndex, _centerIndex);

        _centerIndex = pageIndex;

    }
}
