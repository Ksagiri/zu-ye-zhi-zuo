﻿//----------------- AudioLoop -----------------
//
//      概要 : 曲選択時に曲の一部をループさせる
//      内容 : 曲の一部をループさせる情報をまとめている
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioLoop : MonoBehaviour {

    private AudioSource _audioSource;   // ソース

    private float _startTime = 0;       // 曲を再生した時間

    private float _loopTime = 0;        // ループしている時間

    private float _outTime = 2.0f;      // 曲がフェードアウトしている時間

    private float _delayTime = 1.0f;    // 曲を流すときのディレイタイム

    private bool _playFlg = false;      // 曲を再生する

	// Use this for initialization
	void Awake () 
    {
        _audioSource = GameObject.Find("DataBase").GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        
        if(_playFlg == true)
        {
            // 時間になったらフェードアウトし始める
            // ループ時間を越えたら最初から再生
            if(getMusicTime() >= _loopTime - _outTime)
            {
                _audioSource.volume = (_loopTime - getMusicTime()) / 2;

                // 終了 -> ループ
                if (getMusicTime() >= _loopTime)
                {
                    playStart();
                }
            }

        }
		
	}

    //機能 : ループ再生に必要な情報を入れる
    //引数 : 
    //第1引数 : ループ再生開始時間
    //第2引数 : ループ再生終了時間
    //第3引数 : 曲ファイルパス
    //戻り値 : なし
    public void setInfo(float loopStart, float loopEnd, AudioClip clip)
    {
        // ループ時間
        _loopTime = loopEnd - loopStart + _delayTime;

        // 曲をロード
        _audioSource.clip = clip;
        
        // 曲の開始位置をセット
        _audioSource.time = loopStart;

        // 曲を流す
        playStart();
        _playFlg = true;
    }


    //機能 : 曲の再生停止
    //引数 : プレイフラグ
    //戻り値 : なし
    public void setPlayFlg(bool flg)
    {
        _playFlg = flg;
    }

    //機能 : 曲の開始
    //引数 : なし
    //戻り値 : なし
    public void playStart()
    {
        // 曲を開始する時間
        _startTime = Time.time + _delayTime;
        
        // 曲のボリュームを最大にリセット
        _audioSource.volume = 1.0f;

        // ディレイをかけて曲を流し始める
        _audioSource.PlayDelayed(_delayTime);
    }

    //機能 : 曲開始からどのくらい経ったのかfloat秒単位で返す
    //          ゲームの開始した時間 - 曲を開始した時間 = 曲開始からの時間(秒)
    //引数 : なし
    //戻り値 : 曲開始からの時間
    private float getMusicTime()
    {
        return Time.time - _startTime;
    }

    
}
