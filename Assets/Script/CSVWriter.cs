﻿//----------------- CSVWriter -----------------
//
//      概要 : 譜面データをcsvに書き込む
//      内容 : 特定のフォルダに指定したファイル名のcsvファイルを保存する
//
//      作成者 : 工藤　祐平
//
//


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class CSVWriter : MonoBehaviour {


    public string _fileName = "Resources/CSV/";     //保存するファイル名

	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    //機能:書き込むCSV
    //引数:csvの名前
    //戻り値:なし
    public void writerCSV(string text)
    {
        StreamWriter stream;
        FileInfo fileInfo;
        fileInfo = new FileInfo(Application.dataPath + "/" + _fileName + ".csv");
        stream = fileInfo.AppendText();
        stream.WriteLine(text);
        stream.Flush();
        stream.Close();
    }


}
