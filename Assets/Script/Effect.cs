﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Effect : MonoBehaviour {
    public GameObject _effect;
    private GameObject _particle;

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {

	}



    public void createEffect()
    {
        //スクリーン座標をワールド座標に変換(マウスのx座標,マウスのy座標,Z軸(2Dの場合はカメラの位置が-10の位置にいるので修正する))
        //Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10f));
        Vector3 pos = new Vector3(Const.Consts.COLLISION_POSX, 0, 0);

        //パーティクルを生成する(既存のGameObject,位置,向き(現在はx軸に-90度回転させている))
        _particle = (GameObject)Instantiate(_effect, pos, Quaternion.Euler(new Vector3(-90, 0, 0)));


        //0.5秒後にパーティクルを消す
        Destroy(_particle, 0.5f);
    }
}
