﻿//----------------- NotesCreate -----------------
//
//      概要 : ノーツを作成する
//      内容 : 譜面データ(csv)を読み込み、曲の時間に合わせてノーツを作成する
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;


public class NotesCreate : MonoBehaviour
{

    private AudioSource _audioSource;           //曲
    private NotesCollision _notesCollision;     //ノーツ判定用スクリプト
    private Shake _shake;                   //フィーバータイムをとめるため
    private float[] _notesTime;                 // ノーツが枠に合う時間
    private float _startTime;                   // 楽曲の開始時間
    private float _offset = 2.0f;              // 出現する時間を早めにする
    private float _measureTime = 0;             // 1小節
    private float _beatTime = 0;                // 1拍の時間
    private float _feverTime = 0;               // フィーバータイムに突入する時間
    private float _endTime = 0;                 // プレイ終了時間
    private float _endDelayTime = 1.0f;         // 曲終了からシーン移行するまでの時間(秒)
    private int[] _category;                    // ノーツの種類(タップや左フリックなど)
    private int _notesCount = 0;                // 現在のノーツの数
    private bool _playFlg = false;              // 楽曲をプレイしているかどうか

    private const int ARYNUM = 1024;

    private int[] _debugMeasure;                // デバッグ用の小節
    private int[] _debugBeat;                   // デバッグ用の拍

    public GameObject[] _notes;                 // ノーツのオブジェクト
    public string _filePass;                    // csvファイルの名前

    private GameObject _long;                   // ロングノーツの長い部分のオブジェクト

    public GameObject _longPrefab;              // ロングノーツの長い部分のプレハブ
    public GameObject _countDownObj;
    public bool _debugMode = false;             // デバッグプレイかどうか


    // Use this for initialization
    void Start()
    {
        //曲を取得
        _audioSource = GameObject.Find("DataBase").GetComponent<AudioSource>();
        _shake = GameObject.Find("DataBase").GetComponent<Shake>();

        if(_debugMode == false)
        {
            _audioSource.clip = Audio.getInstance().getAudio(SelectButton.getSelectNumber());
            _filePass = SelectButton.getSelectCSV();
        }

        // SEのロード
        Audio.getInstance().loadSE((int)Const.AudioSE.END_VOICE, Const.AudioSEName.END_VOICE);
        

        _endTime = _audioSource.clip.length + _endDelayTime;

        _notesCollision = GameObject.Find("DataBase").GetComponent<NotesCollision>();
        
        //配列を初期化
        _notesTime = new float[ARYNUM];
        _category = new int[ARYNUM];

        //デバッグ用配列初期化
        _debugMeasure = new int[ARYNUM];
        _debugBeat = new int[ARYNUM];

        //ノーツのロード
        loadCSVData();
        
        // 各判定(Perfectなど)の時間をセット
        _notesCollision.setCollisionsTime(_beatTime * 0.7f,_beatTime * 0.8f,_beatTime * 0.9f,_beatTime);

        Invoke("startMusic", 3.0f);

    }

    // Update is called once per frame
    void Update()
    {
        //楽曲プレイ中
        if (_playFlg == true)
        {

            //譜面のタイミングがきたらノーツ作成
            if (_notesTime[_notesCount] - _offset <= getMusicTime() && _notesTime[_notesCount] != 0)
            {
                //ノーツ作成
                spawnNotes(_category[_notesCount]);
                //次のノーツ
                _notesCount++;
            }

            //曲終了時間に到達
            if (_endTime <= getMusicTime() && _debugMode == false)
            {
                // プレイ終了
                _playFlg = false;

                _shake.setShakeFlg(false);

                // 歓声
                Audio.getInstance().playSE((int)Const.AudioSE.END_VOICE);

                // 歓声が終わり次第シーン移動させる
                StartCoroutine("seAndScene", (int)Const.AudioSE.END_VOICE);
            }
            
        }
    }


    // SEを鳴らし終わってからシーン移動するための処理を実行する
    // コルーチン
    IEnumerator seAndScene(int id)
    {
        while (true)
        {
            // SEが終わったらシーン移動
            if (!Audio.getInstance().getSEPlaying(id))
            {
                GameObject.Find("DataBase").GetComponent<Score>().calcScoreRank();
                GameObject.Find("DataBase").GetComponent<Loading>().nextScene();
                break;
            }

            yield return null;
        }
    }


    //機能 : ノーツ時間のゲッター
    //引数 : なし
    //戻り値 : ノーツの判定ラインの時間
    public float[] getNotesTime()
    {
        return _notesTime;
    }


    //機能 : ノーツ種類のゲッター
    //引数 : なし
    //戻り値 : ノーツの種類
    public int[] getNotesCategory()
    {
        return _category;
    }

    //機能 : デバッグ用の小節ゲッター
    //引数 : なし
    //戻り値 : ノーツの小節値を格納した配列
    public int[] getDebugMeasure()
    {
        return _debugMeasure;
    }

    //機能 : デバッグ用の拍ゲッター
    //引数 : なし
    //戻り値 : ノーツの拍値を格納した配列
    public int[] getDebugBeat()
    {
        return _debugBeat;
    }

    //機能 : 開始(譜面、曲)開始時間のゲッター
    //引数 : なし
    //戻り値 : 開始時間
    public float getStartTime()
    {
        return _startTime;
    }

    //機能 : 曲を開始する
    //引数 : なし
    //戻り値 : なし
    public void startMusic()
    {
        // カウントダウンアニメを破棄する
        Destroy(_countDownObj.gameObject);

        // 開始時間を保存
        _startTime = Time.time;
        _notesCollision.setStartTime(_startTime);

        // フィーバータイムの時間を渡す
        _notesCollision.setFeverTime(_feverTime);

        // 判定クラスの準備をする
        _notesCollision.initFunc();
        _notesCollision.setPlayFlg(true);

        // ゲームプレイ
        _playFlg = true;

        // 曲開始
        _audioSource.Play();
    }


    //機能 : 始点にノーツを作成する
    //引数 : ノーツの種類
    //戻り値 : なし
    private void spawnNotes(int category)
    {

        // インスタンス化
        GameObject obj = Instantiate(_notes[category], new Vector2(20.0f, 0), Quaternion.identity);
        obj.GetComponent<Notes>().setCreate(_notesTime[_notesCount], category, _startTime);

        // 判定リストにセットする
        _notesCollision.setAddNotes(obj);

        // ロングのスタートならロングノーツの塊を判定リストにセットする
        if (category == (int)Const.TapCate.LONG_START)
        {
            // 長いやつ
            GameObject obj2 = Instantiate(_longPrefab, new Vector2(obj.transform.position.x, 0), Quaternion.identity);
            _long = obj2;
            _long.GetComponent<LongNotes>().setCreate(_notesTime[_notesCount], _notesTime[_notesCount + 1], _startTime);

            _notesCollision.setAddLongNotes(obj2);

            // ロングノーツの終点を作成する

            _notesCount++;
            
            // インスタンス化 
            GameObject obj3 = Instantiate(_notes[(int)Const.TapCate.LONG_STOP], new Vector2(20.0f, 0), Quaternion.identity);
            obj3.GetComponent<Notes>().setCreate(_notesTime[_notesCount], (int)Const.TapCate.LONG_STOP, _startTime);

            // 判定リストにセット
            _notesCollision.setAddNotes(obj3);
        }

    }

    //MusicSliderクラスに必要だったため変更しました(今野)
    //private → publicに変更しました(今野)
    //機能 : 曲開始からどのくらい経ったのかfloat秒単位で返す
    //          ゲームの開始した時間 - 曲を開始した時間 = 曲開始からの時間(秒)
    //引数 : なし
    //戻り値 : 曲開始からの時間
    public float getMusicTime()
    {
        return Time.time - _startTime;
    }

    //MusicSliderクラスに必要だったため作成しました(今野)
    //機能：プレイしてるかしてないかフラグを返す
    //引数：なし
    //戻り値：プレイしていればtrue　してなければfalse
    public bool getPlayFlg()
    {
        return _playFlg;
    }

    


    //機能 : CSVファイルに書き込んだ譜面データを取得する
    //引数 : なし
    //戻り値 : なし
    private void loadCSVData()
    {

        // ストリームリーダーsrに読み込む
        //csvファイルを読み込む
        TextAsset csvFile = Resources.Load(_filePass) as TextAsset;

        StringReader sr = new StringReader(csvFile.text);

        //要素数に使う変数
        int i = 0;

        //次の文字(データ)がなければ終了
        while (sr.Peek() > -1)
        {
            //行を取り出す
            string line = sr.ReadLine();
            //行データをカンマ区切りで分けて配列に取り出す
            string[] val = line.Split(',');

            //csvの一行目だった場合一拍の時間を変数に保存する
            if (i == 0)
            {
                _measureTime = float.Parse(val[1]);
                _beatTime = float.Parse(val[2]);

                i++;
                continue;
            }

            //フィーバーならフィーバータイムに突入する時間を保存しループを抜ける
            if (int.Parse(val[2]) == (int)Const.TapCate.SHAKE)
            {
                _feverTime = _measureTime * (int.Parse(val[0]) - 1) + _beatTime * ((int.Parse(val[1]) - 1));
                break;
            }

            //ノーツの判定ラインにくる時間を格納
            _notesTime[i - 1] = _measureTime * (int.Parse(val[0]) - 1) + _beatTime * ((int.Parse(val[1]) - 1));

            //ノーツの種類をint型で格納
            _category[i - 1] = int.Parse(val[2]);

            //デバッグ用に小節と拍もとっておく
            _debugMeasure[i - 1] = int.Parse(val[0]);
            _debugBeat[i - 1] = int.Parse(val[1]);

            //次の要素へ
            i++;
        }
        if(_debugMode == false)
            GameObject.Find("DataBase").GetComponent<Score>().setMaxScore(i-1);
    }
}
