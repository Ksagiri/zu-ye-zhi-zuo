﻿//----------------- SelectClothesButton -----------------
//
//      概要 : 服選択のウィンドウ
//      内容 : 着せ替えをする処理
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public struct SelClothes
{
    public int id;              // 服のID
    public Sprite buttonSprite;  // 服を選択するボタンのスプライト
    public Sprite clothesSprite;  // くまの立ち絵のスプライト
}

public class SelectClothesButton : MonoBehaviour {

    private static int _clothesId = 0;                 //選択した服のID
    private static string _selectAnimName;        //アニメーションの名前

    private string[] _AnimName;            

    private SelClothes[] _selClothes; //選択した服の立ち絵

    private Button[] _button;                   //選択するボタン


    public string _filePass;                    //読み込むcsv

    public GameObject _clothesPanel;              //難易度選択のパネル
    public GameObject _clothesImage;           //着せ替えしたものを表示させる画像
    public Button _buttonPre;                   //ボタンのプレハブ


    // Use this for initialization
    void Start()
    {
        int i = 0;

        // SEのロード
        Audio.getInstance().loadSE((int)Const.AudioSE.SELECT, Const.AudioSEName.SELECT);
        Audio.getInstance().loadSE((int)Const.AudioSE.CANCEL, Const.AudioSEName.CANCEL);

        //立ち絵配列を準備
        _selClothes = new SelClothes[Const.Consts.CLOTHES];

        _AnimName = new string[Const.Consts.CLOTHES];

        //csvのロード
        loadCSVData();

        //デフォルトの立ち絵を設定
        _clothesImage.GetComponent<Image>().sprite = _selClothes[0].clothesSprite;
        
        //服選択パネルの初期設定を行う
        _clothesPanel.SetActive(false);

        //contentを取得
        RectTransform content = GameObject.Find("Canvas/Panel/SelectPanel/ClothesPanel/Image/ScrollViewClothes/Viewport/Content").GetComponent<RectTransform>();
        RectTransform scrView = GameObject.Find("Canvas/Panel/SelectPanel/ClothesPanel/Image/ScrollViewClothes").GetComponent<RectTransform>();


        //ボタンの間隔
        float btnSpace = content.GetComponent<GridLayoutGroup>().spacing.y;

        //ボタンの幅
        float btnWidth = _buttonPre.GetComponent<LayoutElement>().preferredWidth;

        //ボタンの高さ
        float btnHeight = _buttonPre.GetComponent<LayoutElement>().preferredHeight;

        //contentの上端と下端から端のボタンへの距離を設定する
        int centerPosX = (int)scrView.sizeDelta.x / 2 - (int)btnWidth / 2;
        int centerPosY = (int)scrView.sizeDelta.y / 2 - (int)btnHeight / 2;
        content.GetComponent<GridLayoutGroup>().padding.left = centerPosX;
        content.GetComponent<GridLayoutGroup>().padding.right = centerPosX;
        content.GetComponent<GridLayoutGroup>().padding.top = centerPosY;
        content.GetComponent<GridLayoutGroup>().padding.bottom = centerPosY;


        //contentの幅
        //(ボタンの幅 + ボタンの間隔) * ボタンの数
        content.sizeDelta = new Vector2(0, (btnHeight + btnSpace) * Const.Consts.CLOTHES);

        //ボタンを作る
        _button = new Button[Const.Consts.CLOTHES];

        //ボタンの生成を行う
        for (i = 0; i < _button.Length; i++)
        {
            int no = i;
            //ボタンを生成インスタンス化
            _button[i] = (Button)Instantiate(_buttonPre);

            //ボタンをContentの子どもに設定
            _button[i].transform.SetParent(content, false);


            //ボタンのテクスチャを変更
            _button[i].GetComponentInChildren<Image>().sprite = _selClothes[i].buttonSprite;


            //ボタンのクリックイベントを登録
            //ナンバーを割り当てる
            _button[i].transform.GetComponent<Button>().onClick.AddListener(() => onClickClothes(no));
        }

        //真ん中の９ボタンだけ効果を発揮する
        setInterctive(false);
        //Debug.Log("初期服ID : " + _clothesId);
        _button[_clothesId].interactable = true;
        _clothesImage.GetComponent<Image>().sprite = _selClothes[_clothesId].clothesSprite;
        _selectAnimName = _AnimName[_clothesId];

    }

    // Update is called once per frame
    void Update()
    {

    }

    //機能 : 服選択パネルを有効化する
    //引数 : なし
    //戻り値 : なし
    public void onClickClothesPanel()
    {
        //服選択のパネルを有効化
        _clothesPanel.SetActive(true);
    }


    //機能 : 服選択パネルを閉じる
    //引数 : なし
    //戻り値 : なし
    public void onClickClothesClose()
    {
        Audio.getInstance().playSE((int)Const.AudioSE.CANCEL);

        //服選択のパネルを無効化
        _clothesPanel.SetActive(false);
    }


    //機能 : 服の選択(着せ替え)
    //引数 : なし
    //戻り値 : なし
    public void onClickClothes(int no)
    {
        Audio.getInstance().playSE((int)Const.AudioSE.SELECT);

        //服のIDを保存
        _clothesId = no;

        _selectAnimName = _AnimName[no];

        //服を着せ替える
        _clothesImage.GetComponent<Image>().sprite = _selClothes[no].clothesSprite;

    }

    //機能 : 選択した衣装のIDを返す
    //引数 : なし
    //戻り値：選択した衣装のID
    public static int getClothesId()
    {
        return _clothesId;
    }

    public static string getSelectAnimName()
    {
        return _selectAnimName;
    }

    //ボタンを有効化・無効化する
    //引数：false(無効化) true(有効化)
    //戻り値：なし
    public void setInterctive(bool isInteractive)
    {
        //各ボタンを引数に応じて有効化・無効化する
        for (int i = 0; i < _button.Length; i++)
        {
            _button[i].interactable = isInteractive;
        }
    }

    // 真ん中のボタン機能を使えるようにする
    // 引数:
    // 第一引数 : 真ん中のボタンのID
    // 第二引数 : 元真ん中のボタンのID
    // 戻り値 : なし
    public void centerButtonActive(int centerID, int srcID)
    {
        // ボタン機能を使えるようにする
        // 元のボタンは使えないようにする
        _button[srcID].interactable = false;
        _button[centerID].interactable = true;

        _clothesImage.GetComponent<Image>().sprite = _selClothes[centerID].clothesSprite;
        _selectAnimName = _AnimName[centerID];
        _clothesId = centerID;

    }

    //機能 : CSVファイルに書き込んだ譜面データを取得する
    //引数 : なし
    //戻り値 : なし
    private void loadCSVData()
    {
        //Debug.Log(_filePass);

        // ストリームリーダーsrに読み込む
        //csvファイルを読み込む
        TextAsset csvFile = Resources.Load(_filePass) as TextAsset;

        StringReader sr = new StringReader(csvFile.text);

        //要素数に使う変数
        int i = 0;

        //次の文字(データ)がなければ終了
        while (sr.Peek() > -1)
        {
            //行を取り出す
            string line = sr.ReadLine();
            //行データをカンマ区切りで分けて配列に取り出す
            string[] val = line.Split(',');

            //服情報にIDを割り当て
            _selClothes[i].id = i;


            //csvに保存されている着せ替えの情報を保存
            
            //着せ替えボタンの文字
            _selClothes[i].buttonSprite = Resources.Load("Text/" + val[0], typeof(Sprite)) as Sprite;
            //着せ替えするときの立ち絵
            _selClothes[i].clothesSprite = Resources.Load("Sprite/" + val[0], typeof(Sprite)) as Sprite;
            //アニメーションの名前を保存
            _AnimName[i] = val[0];

            //次の要素へ
            i++;
        }
    }
}
