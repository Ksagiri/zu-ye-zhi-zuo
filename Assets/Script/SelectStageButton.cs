﻿//----------------- SelectStageButton -----------------
//
//      概要 : ステージ選択のボタン管理
//      内容 : ボタンの作成や操作による処理
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;


public class SelectStageButton : MonoBehaviour
{
    private Button[] _button;               //選択するボタン

    private string[] _stageName;            //ステージの名前が入っている
    private Sprite[] _stageSprite;          //ステージのスプライトが入っている
    private int _selectNumber = 0;          //選ばれたステージナンバー
    private static int _stageSendId = 0;    //プレイシーンに送るステージID
    private static string _stageSpritePass; //プレイシーンに送るステージスプライトのパス

    public Image _stage;                    //表示されるステージの画像をいれるやつ

    public Button _buttonPre;               //ボタンのプレハブ
    public string _filePass;                //csvファイルのパス

    // Use this for initialization
    void Start()
    {
        int i = 0;

        _selectNumber = _stageSendId;

        //ステージのパス
        //_stageName = new string[STAGE_NUM] { "うみ", "ディスコ", "冬の街", "祭り", "遊園地" };
        _stageName = new string[Const.Consts.STAGE];
        
        //ステージのスプライト
        _stageSprite = new Sprite[Const.Consts.STAGE];

        loadCSVData();

        //contentを取得
        RectTransform content = GameObject.Find("Canvas/Panel/ScrollView/Viewport/Content").GetComponent<RectTransform>();
        RectTransform scrView = GameObject.Find("Canvas/Panel/ScrollView").GetComponent<RectTransform>();


        //ボタンの間隔
        float btnSpace = content.GetComponent<GridLayoutGroup>().spacing.x;

        //ボタンの幅
        float btnWidth = _buttonPre.GetComponent<LayoutElement>().preferredWidth;


        //ボタンの高さ
        float btnHeight = _buttonPre.GetComponent<LayoutElement>().preferredHeight;

        //contentの左端と右端から端のボタンへの距離を設定する
        int centerPosX = (int)scrView.sizeDelta.x / 2 - (int)btnWidth / 2;
        int centerPosY = (int)scrView.sizeDelta.y / 2 - (int)btnHeight / 2;
        content.GetComponent<GridLayoutGroup>().padding.left = centerPosX;
        content.GetComponent<GridLayoutGroup>().padding.right = centerPosX;
        content.GetComponent<GridLayoutGroup>().padding.top = centerPosY;


        //contentの幅
        //(ボタンの幅 + ボタンの間隔) * ボタンの数
        content.sizeDelta = new Vector2((btnWidth + btnSpace) * Const.Consts.STAGE, 256);

        //ボタンを作る
        _button = new Button[Const.Consts.STAGE];

        //ボタンの生成を行う
        for (i = 0; i < _button.Length; i++)
        {
            int no = i;

            //ステージのスプライトをロードする
            _stageSprite[i] = Resources.Load("Stage/" + _stageName[i] + "_stage", typeof(Sprite)) as Sprite;
            
            //ボタンを生成インスタンス化
            _button[i] = (Button)Instantiate(_buttonPre);

            //ボタンをContentの子どもに設定
            _button[i].transform.SetParent(content, false);


            //ボタンのテキスト(テクスチャを変更)
            //テストとして作った順番に番号を表示
            _button[i].GetComponentInChildren<Image>().sprite = Resources.Load("Text/" + _stageName[i] + "_text", typeof(Sprite)) as Sprite;

            //ボタンのクリックイベントを登録
            //ナンバーを割り当てる
            _button[i].transform.GetComponent<Button>().onClick.AddListener(() => setStageSprite(no));
        }

        //真ん中のボタンだけ効果を発揮する
        setInterctive(false);
        _button[_selectNumber].interactable = true;
        setStageSprite(_selectNumber);

    }

    // Update is called once per frame
    void Update()
    {

    }


    //ボタンを有効化・無効化する
    //引数：false(無効化) true(有効化)
    //戻り値：なし
    public void setInterctive(bool isInteractive)
    {
        //各ボタンを引数に応じて有効化・無効化する
        for (int i = 0; i < _button.Length; i++)
        {
            _button[i].interactable = isInteractive;
            
        }
    }

    // 真ん中のボタンのサイズを大きくしてボタン機能を使えるようにする
    // 引数:
    // 第一引数 : 真ん中のボタンのID
    // 第二引数 : 元真ん中のボタンのID
    // 戻り値 : なし
    public void centerButtonActive(int centerID, int srcID)
    {
        // ボタン機能を使えるようにする
        // 元のボタンは使えないようにする
        _button[srcID].interactable = false;
        _button[centerID].interactable = true;

        setStageSprite(centerID);

    }

    //ステージのスプライトをセット
    //引数：ボタンのId
    private void setStageSprite(int id)
    {
        _stage.sprite = _stageSprite[id];
        _stageSendId = id;

        _stageSpritePass = _stageName[id] + "_stage";

        //曲のナンバーを保存する
        _selectNumber = id;
    }

    //ステージのIDを送る
    public static int getStageId()
    {
        return _stageSendId;
    }

    public static string getSpritePass()
    {
        return _stageSpritePass;
    }

    //機能 : CSVファイルに書き込んだ譜面データを取得する
    //引数 : なし
    //戻り値 : なし
    private void loadCSVData()
    {

        // ストリームリーダーsrに読み込む
        //csvファイルを読み込む
        TextAsset csvFile = Resources.Load(_filePass) as TextAsset;

        StringReader sr = new StringReader(csvFile.text);

        //要素数に使う変数
        int i = 0;

        //次の文字(データ)がなければ終了
        while (sr.Peek() > -1)
        {
            //行を取り出す
            string line = sr.ReadLine();
            //行データをカンマ区切りで分けて配列に取り出す
            string[] val = line.Split(',');

            //ステージがないなら処理しない
            if(i != 0)
            {
                _stageName[i-1] = val[1];
            }

            //次の要素へ
            i++;
            
        }
    }
}
