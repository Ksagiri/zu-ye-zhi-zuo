﻿//----------------- NotesMaker -----------------
//
//      概要 : 譜面作成するクラス
//      内容 : csvに書き込むデータ
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotesMaker : MonoBehaviour {

    private InputManager _input;
    private AudioSource _audioSource;           //AudioSourceの変数
    private CSVWriter   _writer;                //csv読み込みのクラス変数
    private List<GameObject> _line;        //生成された線を格納
    private int         _beat = 1;              //拍
    private int         _measure = 1;           //小節
    private float       _startTime = 0;         //曲が始まった時間
    private float       _beatTime = 0;              //一拍の秒数
    private float       _measureTime = 0;           //1小節の秒数
    private float       _sumTime = 0;           //曲が始まってからの拍子区切りの合計時間
    private float       _createTime = 0;        //リズムに合わせて作成する線の時間
    private bool        _playingFlg = false;    //曲作成中:true

    public AudioClip _tempoSE;
    public AudioClip _tapSE;
    public GameObject _startButton;             //作成を開始するボタン
    public GameObject[] _lineSprite;                  //
    public float _offset = -3.0f;        //線を作成する時間を何秒前にするのか
    public float _bpm = 0;                      //曲のbpmを指定
    public float _division = 1;             //拍子を何分割で保存するか


    //csvに時間を書き込む際に何の時間データなのか判断するためのカテゴリー
    //enum Const.TapCate
    //{
    //    TAP,
    //    FLICK_LEFT,
    //    FLICK_RIGHT,
    //    LONG_START,
    //    LONG_STOP,
    //    SHAKE
    //}

    //csvに書き込む際に線でタイミングを取るので小節と拍の線のためのカテゴリー
    enum Line
    {
        BEAT,
        MEASURE
    }


	// Use this for initialization
	void Start () 
    {
        _input = GameObject.Find("DataBase").GetComponent<InputManager>();
        _audioSource = GameObject.Find("DataBase").GetComponent<AudioSource>();
        _writer = GameObject.Find("DataBase").GetComponent<CSVWriter>();
        _line = new List<GameObject>();


        //一拍の秒数を求める
        _beatTime = 60.0f / _bpm / _division;

        Debug.Log("一拍の時間" + _beatTime);

        //指定された分割数と四拍で一小節のノーツ数を決める
        _division *= 4;


        //1小節の秒数を求める
        _measureTime = _beatTime * _division;

        Debug.Log("一小節の時間" + _measureTime);

        //最初の線の時間
        _createTime = _beatTime;

        //csvの一行目に「BPM」「1小節の時間」「一拍の時間」
        _writer.writerCSV(_bpm.ToString() + "," + _measureTime.ToString() + "," + _beatTime.ToString());

	}
	
	// Update is called once per frame
	void Update () 
    {
        //譜面作成可能状態なら
		if(_playingFlg == true)
        {
            
            //拍子分割秒毎に合計時間を増やしていく
            if (getMusicTime() - _sumTime>= _beatTime)
            {
                GetComponent<AudioSource>().PlayOneShot(_tempoSE);
                //一拍秒加算
                _sumTime += _beatTime;
                _beat++;

                //小節の最後なら次小節、一拍目にする
                if (_beat % _division == 1)
                {
                    _measure++;
                    _beat = 1;
                }
            }

            //小節と拍のタイミングより一定時間前にラインオブジェクトを作成する
            if (_createTime + _offset <= getMusicTime() || _createTime + _offset < 0)
            {

                //ライン作成
                spawnNotes(_createTime, (int)Line.BEAT);

                //次に作りたい線の時間を加算(一拍分)
                _createTime += _beatTime;
            }

            //時間経過した線は消す
            for (int i = _line.Count-1; i >= 0 ; i--)
            {
                if (_line[i] != null && _line[i].GetComponent<Notes>().getNotesTime() + _beatTime * 2 <= getMusicTime())
                {
                    Destroy(_line[i].gameObject);
                }
            }

            //入力判定
            detectTap();
        }
	}

    //機能 : 曲を流し、譜面の書き込みを許可する
    //引数 : なし
    //戻り値 : なし
    public void StartMusic()
    {
        _startButton.SetActive(false);
        _audioSource.Play();
        _startTime = Time.time;
        _playingFlg = true;

    }

    //機能 : 譜面作成終了
    //引数 : なし
    //戻り値 : なし
    public void StopMusic()
    {
        _playingFlg = false;
        
        _startButton.SetActive(true);
        Debug.Log("終了");
    }

    //機能 : 譜面作成の入力受付
    //引数 : なし
    //戻り値 : なし
    private void detectTap()
    {
        
        //現在はテスト用にスペースキー
        //本番はInputManagerのそれぞれのタップ判定を使うと思う


        //スペースキー    :   タップしたとき
        if (Input.GetKeyDown(KeyCode.Space) || _input.clickScreen())
        {
            //Debug.Log("space");
            writeTimingDiffernce(getMusicTime(), (int)Const.TapCate.TAP);
        }

        //左矢印   :   左フリック
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            writeTimingDiffernce(getMusicTime(), (int)Const.TapCate.FLICK_LEFT);
        }

        //右矢印   :   右フリック
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            writeTimingDiffernce(getMusicTime(), (int)Const.TapCate.FLICK_RIGHT);
        }

        //上矢印   :   長押しの始まり
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            writeTimingDiffernce(getMusicTime(), (int)Const.TapCate.LONG_START);
        }

        //下矢印   :   長押しの終わり
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            writeTimingDiffernce(getMusicTime(), (int)Const.TapCate.LONG_STOP);
        }


        //下矢印   :   長押しの終わり
        if (Input.GetKeyDown(KeyCode.Return))
        {
            writeTimingDiffernce(getMusicTime(), (int)Const.TapCate.SHAKE);
        }
    }


    //機能 : 入力に対応し前のノーツとの時間差(単位秒)をcsvファイルに書き込む
    //引数 : 第一引数 タップした時間   第二引数 ノーツの種類
    //戻り値 : なし
    private void writeTimingDiffernce(float time, int category)
    {
        GetComponent<AudioSource>().PlayOneShot(_tapSE);
        //現在のタイミングに一番近い拍子に入れる
        //前テンポの時間との差
        float preTime = Mathf.Abs(time - _sumTime);
        //次テンポの時間との差
        float afterTime = Mathf.Abs(_sumTime + _beatTime - time);
        int measure = _measure;
        int beat = _beat;


        //近いテンポのタイムを保存したいので次テンポに近い場合は調整する
        if(preTime > afterTime)
        {
            if (++beat == _division + 1)
            {
                measure++;
                beat = 1;
            }
        }

        //二行目以降
        //書き込み
        _writer.writerCSV(measure.ToString() + "," + beat.ToString() + "," + category.ToString());


        Debug.Log("書き込み: 小節->" + measure + "拍->" + beat);

    }


    //現在の時間
    private float getMusicTime()
    {
        return Time.time - _startTime;
    }


    //機能 : 始点にノーツを作成する
    //引数 : 判定ラインの時間, ノーツの種類
    //戻り値 : なし
    private void spawnNotes(float time,int category)
    {
        //インスタンス化
        GameObject obj = Instantiate(_lineSprite[category], new Vector2(12.0f, 0), Quaternion.identity);
        obj.GetComponent<Notes>().setCreate(time, category, _startTime);
        _line.Add(obj);
    }
}
