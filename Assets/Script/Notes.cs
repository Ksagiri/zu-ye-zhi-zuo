﻿//----------------- Notes -----------------
//
//      概要 : ノーツの処理を行う
//      内容 : ノーツの動き、速度、判定を行う
//              譜面作成での線にも使用する
//
//      作成者 : 今野咲霧
//
//
//      追加,変更 : 2017/10/26～ : 工藤祐平



using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notes : MonoBehaviour {

    private float _notesTime;               //このノーツが枠につくタイミング
    private float _startTime = 0;           //開始時間
    private int _notesCate;                 //このノーツの種類
    private bool _stopFlg = false;          // ロング始点の場合判定バーのところで止める処理に使う
    private bool _notesStartFlg = false;    // 同上

    public float _speed = 0.4f;             //ノーツの速さ
    public int _lineLen = 12;               //レーンの長さ

	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
        //移動処理
        if (_stopFlg == false)
        {
            this.transform.position = new Vector3(move(1, _notesTime, getMusicTime()), 0, 0);
        }
        //ストップするノーツが判定バーにきたとき
        if(_notesStartFlg == true && this.transform.position.x < Const.Consts.COLLISION_POSX)
        {
            _stopFlg = true;
            this.transform.position = new Vector3(Const.Consts.COLLISION_POSX, 0, 0);
        }
    }

    //ノーツの移動処理
    //引数
    //第1引数 終点から始点への方向 
    //第2引数 判定のラインにくる時間 
    //第3引数 曲の再生時間(再生してから何秒たったのか) 
    //戻り値：float型　計算結果が戻り値になる
    public float move(int dir, float lineTime, float playTime)
    {
        //終点の位置 + 終点から始点への方向 * ( ( 判定ラインにくる時間 - 曲の再生時間 ) * レーンの長さ * スピード)
        return Const.Consts.COLLISION_POSX + dir * ((lineTime - playTime) * _lineLen * _speed);
    }



    //機能 : ノーツ作成されるときに必要な時間データを渡す
    //引数
    //第1引数 : ノーツが判定ラインにくる時間
    //第2引数 : ノーツの種類
    //第3引数 : 曲開始からの時間
    //戻り値 : なし
    public void setCreate(float time, int category, float startTime)
    {
        _notesTime = time;
        _notesCate = category;
        _startTime = startTime;
    }

    //機能 : 判定の枠でストップする特殊ノーツのセッター
    //      ロングノーツの始まりやフィーバーに移るノーツなど
    //引数 : bool
    //戻り値 : なし
    public void setNotesStartFlg(bool flg)
    {
        _notesStartFlg = flg;
    }

    //機能 : ノーツが判定ラインにくる時間のゲッター
    //引数 : なし
    //戻り値 : ノーツの判定する時間
    public float getNotesTime()
    {
        return _notesTime;
    }

    //機能 : ノーツ種類のゲッター
    //引数 : なし
    //戻り値 : ノーツの種類
    public int getNotesCategory()
    {
        return _notesCate;
    }


    //機能 : 曲開始からどのくらい経ったのかfloat秒単位で返す
    //          ゲームの開始した時間 - 曲を開始した時間 = 曲開始からの時間(秒)
    //引数 : なし
    //戻り値 : 曲開始からの時間
    private float getMusicTime()
    {
        return Time.time - _startTime;
    }

}
