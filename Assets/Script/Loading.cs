﻿//----------------- Loading -----------------
//
//      概要 : ローディング画面を生成
//      内容 : シーンをロードする間ローディング画面を表示する
//
//      作成者 : 工藤　祐平
//
//


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour {

    private AsyncOperation _async;  //ロードの進行度を知りたい
    public GameObject _loadingUI;   //ローディングパネル
    
    public Slider _slider;          //ロードのゲージ
    public string _nextSceneName;   //ロードするシーン名

    public bool _ownerPanel = true;


	// Use this for initialization
	void Start () 
    {
        // ローディングパネルを所有している場合
        if(_ownerPanel == true)
        {
            //ローディングパネルを無効化
            _loadingUI.SetActive(false);
        }
        

        Audio.getInstance().loadSE((int)Const.AudioSE.OK, Const.AudioSEName.OK);
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void nextScene()
    {
        SceneManager.LoadScene(_nextSceneName);
    }

    //機能 : ローディング画面を表示してロードするコルーチンを開始
    //引数 : なし
    //戻り値：なし
    public void loadNextScene()
    {
        //SEを鳴らす
        Audio.getInstance().playSE((int)Const.AudioSE.OK);

        StartCoroutine("seAndScene", (int)Const.AudioSE.OK);

        //ローディングパネルを有効化
        _loadingUI.SetActive(true);
    }

    IEnumerator seAndScene(int id)
    {
        while (true)
        {
            if (!Audio.getInstance().getSEPlaying(id))
            {
                StartCoroutine(loadScene());
                break;
            }

            yield return null;
        }
    }

    //機能 : シーンをロードする
    //引数 : なし
    //戻り値：なし
    IEnumerator loadScene()
    {
        _async = SceneManager.LoadSceneAsync(_nextSceneName);

        while(!_async.isDone)
        {
            //ゲージをロードの進行度に合わせて進める
            _slider.value = _async.progress;
            yield return null;
        }
    }
}
