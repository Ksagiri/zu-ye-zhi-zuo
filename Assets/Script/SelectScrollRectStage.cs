﻿//----------------- SelectScrollRectStage -----------------
//
//      概要 : ステージ選択のスクロール管理
//              ScrollRectの拡張版なので、対象のScrollViewにつける
//      内容 : スクロール処理
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectScrollRectStage : ScrollRect
{

    private float _pageWidth;                   //1ページ(ボタン)の幅

    private int _centerIndex = 0;        //前回の真ん中のボタン値,最左が0

    private SelectStageButton _selectButton;    //セレクトステージのクラス

    //初期
    protected override void Awake()
    {
        base.Awake();

        _selectButton = GameObject.Find("DataBase").GetComponent<SelectStageButton>();

        initFunc();
    }

    public void initFunc()
    {
        GridLayoutGroup grid = content.GetComponent<GridLayoutGroup>();

        _centerIndex = SelectStageButton.getStageId() * -1;

        //1ページの幅(ボタンの横幅)を取得
        _pageWidth = grid.cellSize.x + grid.spacing.x;

        //Contentのスクロール位置を決定する
        float destX = _centerIndex * _pageWidth;
        content.anchoredPosition = new Vector2(destX, content.anchoredPosition.y);
    }

    //ドラッグ開始
    public override void OnBeginDrag(PointerEventData eventData)
    {
        base.OnBeginDrag(eventData);

        //すべてのボタンを無効化する
        _selectButton.setInterctive(false);
    }

    //ドラッグ終了
    public override void OnEndDrag(PointerEventData eventData)
    {
        base.OnEndDrag(eventData);

        //ドラッグ終了時にスクロールをとめる
        //スナップさせる位置が決まったあとに慣性を残さないため
        StopMovement();

        //スナップさせる位置を決める
        //スナップさせるボタンの値を決める
        int pageIndex = Mathf.RoundToInt(content.anchoredPosition.x / _pageWidth);

        //ページが変わっていないかつ、素早くドラッグした場合
        if(pageIndex == _centerIndex && Mathf.Abs(eventData.delta.x) >= 5)
        {
            pageIndex += (int)Mathf.Sign(eventData.delta.x);
        }

        //Contentのスクロール位置を決定する
        float destX = pageIndex * _pageWidth;
        content.anchoredPosition = new Vector2(destX, content.anchoredPosition.y);

        //ページが変わっていない判定を行うため前回スナップされていた位置を記憶しておく
        //ページ外に出た場合その値が保存されてしまうため対策としてありえないページは保存しない
        if (pageIndex > 0)
            pageIndex = 0;
        else if (pageIndex * -1 >= Const.Consts.STAGE)
            pageIndex = -(Const.Consts.STAGE - 1);

        //中央のボタンのみ有効化
        _selectButton.centerButtonActive(pageIndex * -1, _centerIndex * -1);

        _centerIndex = pageIndex;

    }
}
