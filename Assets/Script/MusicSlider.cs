﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicSlider : MonoBehaviour {
    public Slider _slider;          //スライダー
    private NotesCreate _notes;     //ノーツクリエイト
    private AudioSource _audio;     //曲情報

	// Use this for initialization
	void Start () {
        _notes = GameObject.Find("DataBase").GetComponent<NotesCreate>();
        _audio = GameObject.Find("DataBase").GetComponent<AudioSource>();

        //曲の長さ(時間)を代入
        _slider.maxValue = _audio.clip.length;
	}
	
	// Update is called once per frame
	void Update () {
        if (_notes.getPlayFlg() == true)
        {
            //現在の曲の再生時間を代入
            _slider.value = _notes.getMusicTime();
        }
	}
}
