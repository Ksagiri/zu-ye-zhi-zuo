﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title : MonoBehaviour {

    public GameObject _creditPanel;

	// Use this for initialization
	void Start () 
    {
        _creditPanel.SetActive(false);	
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    public void onClickCredit()
    {
        _creditPanel.SetActive(true);	
    }

    public void onClickCreditClose()
    {
        _creditPanel.SetActive(false);
    }
}
