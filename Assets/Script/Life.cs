﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Life : MonoBehaviour {
    private GameObject []_texture;      //テクスチャ配列
    private int _life = 3;              //体力
    public GameObject _prefab;          //インスタンス化したやつがはいる
    public Sprite[] _lifeTex;           //3種類のライフテクスチャが入っている
    public float[] _pos;                //ライフの位置


	// Use this for initialization
	void Start () {
        _texture = new GameObject[_life];

        //位置を設定
        _pos = new float[3] { -2.2f, -1.5f, -0.8f };

        //インスタンス化
        for (int i = 0; i < _life; i++)
        {
            _texture[i] = Instantiate(_prefab, new Vector2(_pos[i], 4.0f), Quaternion.identity);     
        }
	}
	
	// Update is called once per frame
	void Update () {
        //テクスチャをセットする
        setTexture(_texture);
	}

    //機能：ライフのゲッター
    //引数：なし
    //戻り値：なし
    public int getLife()
    {
        return _life;
    }

    //機能：ライフが減るとライフの画像も見えないようにする
    //引数：増減する値を入れる
    //戻り値：なし
    public void setLife(int life)
    {
        _life += life;
        if (_life < 0)
            _life = 0;
        _texture[_life].SetActive(false);
    }

    //テクスチャを設定
    //引数：テクスチャのオブジェクト配列
    //戻り値：なし
    public void setTexture(GameObject[] texture)
    {
        //ライフの数に応じてループし、ライフの画像を変更する
        for(int i = 0; i < _life; i++)
        {
            if(_life > 0)
            {
                texture[i].GetComponent<SpriteRenderer>().sprite = _lifeTex[_life - 1];
            }
            
        }
    }

    //ライフの画像を全部見えないようにする
    //引数：なし
    //戻り値：なし
    public void lifeEnable()
    {
        for(int i = 0; i < _life; i++)
        {
            _texture[i].SetActive(false);
        }
    }
}
