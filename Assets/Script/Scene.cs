﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Scene : MonoBehaviour {
    public string _nextSceneName;   //移動したいシーンを入力する

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    //シーン移動
    public void nextScene()
    {
        SceneManager.LoadScene(_nextSceneName);
    }
}
