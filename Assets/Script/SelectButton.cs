﻿//----------------- SelectButton -----------------
//
//      概要 : 曲選択のボタン管理
//      内容 : ボタンの作成や操作による処理
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using UnityEngine.UI;

public struct SelMusic
{
    public int id;              // 曲のID
    public string fileName;     // 曲ファイルのパス
    public string csvName;      // 曲の譜面パス
    public string musicName;    // 表示する曲の名前
    public AudioClip clip;      // 曲
    public Sprite musicSprite;  // 楽曲の名前が書かれているスプライト
    public float loopStartTime; // 曲のループしたい開始時間
    public float loopEndTime;   // 曲のループ終了時間
}


public class SelectButton : MonoBehaviour {

    private AudioLoop _audioLoop;
    private AudioClip[] _audioLoad;
    private static int _selectNumber = 0;       //選ばれた曲ナンバー


    private static string _selectMusicFileName; //選択した曲のファイル名
    private static string _selectCsv;           //選択した曲の譜面
    private static string _selectName;          //選択した曲の表示する名前

    public SelMusic[] _selMusic;

    private Button[] _button;                   //選択するボタン
    public string _filePass;                    //読み込むcsv
    public string[] _levelText;                 //難易度を判断するcsvの後ろの文字
    public Button[] _levelButton;               //難易度のボタン
    public Button _close;                       //難易度選択を閉じるボタン

    public GameObject _levelPanel;              //難易度選択のパネル
    public Button _buttonPre;                   //ボタンのプレハブ


	// Use this for initialization
	void Start () 
    {
        int i = 0;

        // SEのロード
        Audio.getInstance().loadSE((int)Const.AudioSE.SELECT, Const.AudioSEName.SELECT);
        Audio.getInstance().loadSE((int)Const.AudioSE.CANCEL, Const.AudioSEName.CANCEL);
        
        // 曲のループ再生
        _audioLoop = GameObject.Find("DataBase").GetComponent<AudioLoop>();
        _selMusic = new SelMusic[Const.Consts.MUSIC];


        _audioLoad = new AudioClip[Const.Consts.MUSIC];
        //csvのロード
        loadCSVData();


        //難易度選択パネルの初期設定を行う
        _levelPanel.SetActive(false);
        //難易度選択ボタンにボタンイベントを追加する
        for(i = 0; i < _levelButton.Length;i++)
        {
            int no = i;
            _levelButton[i].transform.GetComponent<Button>().onClick.AddListener(() => onClickLevel(no));
        }
        //閉じるボタンにボタンイベントを追加する
        _close.transform.GetComponent<Button>().onClick.AddListener(() => onClickLevelClose());

        //contentを取得
        RectTransform content = GameObject.Find("Canvas/Panel/SelectPanel/ScrollViewMusic/Viewport/Content").GetComponent<RectTransform>();
        RectTransform scrView = GameObject.Find("Canvas/Panel/SelectPanel/ScrollViewMusic").GetComponent<RectTransform>();

        
        //ボタンの間隔
        float btnSpace = content.GetComponent<GridLayoutGroup>().spacing.x;
        
        //ボタンの幅
        float btnWidth = _buttonPre.GetComponent<LayoutElement>().preferredWidth;


        //ボタンの高さ
        float btnHeight = _buttonPre.GetComponent<LayoutElement>().preferredHeight;

        //contentの左端と右端から端のボタンへの距離を設定する
        int centerPosX = (int)scrView.sizeDelta.x / 2 - (int)btnWidth / 2;
        int centerPosY = (int)scrView.sizeDelta.y / 2 - (int)btnHeight / 2;
        content.GetComponent<GridLayoutGroup>().padding.left = centerPosX;
        content.GetComponent<GridLayoutGroup>().padding.right = centerPosX;
        content.GetComponent<GridLayoutGroup>().padding.top = centerPosY;


        //contentの幅
        //(ボタンの幅 + ボタンの間隔) * ボタンの数
        
        content.sizeDelta = new Vector2((btnWidth + btnSpace) * Const.Consts.MUSIC, 256);

        //ボタンを作る
        _button = new Button[Const.Consts.MUSIC];

        //ボタンの生成を行う
        for(i = 0; i < _button.Length; i++)
        {
            int no = i;
            //ボタンを生成インスタンス化
            _button[i] = (Button)Instantiate(_buttonPre);

            //ボタンをContentの子どもに設定
            _button[i].transform.SetParent(content,false);


            //ボタンのテクスチャを変更
            _button[i].GetComponentInChildren<Image>().sprite = _selMusic[i].musicSprite;


            //ボタンのクリックイベントを登録
            //ナンバーを割り当てる
            _button[i].transform.GetComponent<Button>().onClick.AddListener(() => onClickLevelPanel(no));
        }

        //真ん中の９ボタンだけ効果を発揮する
        setInterctive(false);
        _button[_selectNumber].interactable = true;

        _audioLoop.setInfo(_selMusic[_selectNumber].loopStartTime, _selMusic[_selectNumber].loopEndTime, Audio.getInstance().getAudio(_selectNumber));
    }
	
	// Update is called once per frame
	void Update () 
    {
            
	}

    //機能 : 曲選択をしたときに難易度選択パネルを有効化する
    //引数 : なし
    //戻り値 : なし
    public void onClickLevelPanel(int no)
    {
        Audio.getInstance().playSE((int)Const.AudioSE.SELECT);

        //難易度選択のパネルを有効化
        _levelPanel.SetActive(true);

        //曲のナンバーを保存する
        _selectNumber = no;
    }


    //機能 : 難易度選択パネルを閉じる
    //引数 : なし
    //戻り値 : なし
    public void onClickLevelClose()
    {
        Audio.getInstance().playSE((int)Const.AudioSE.CANCEL);
        _levelPanel.SetActive(false);
    }


    //機能 : 難易度選択
    //引数 : なし
    //戻り値 : なし
    public void onClickLevel(int no)
    {
        Audio.getInstance().playSE((int)Const.AudioSE.SELECT);


        _selectMusicFileName = _selMusic[_selectNumber].fileName;
        _selectCsv = _selMusic[_selectNumber].csvName + _levelText[no];
        _selectName = _selMusic[_selectNumber].musicName;

        //次のシーンへ移行する
        GameObject.Find("DataBase").GetComponent<Loading>().loadNextScene();
    }

    public static int getSelectNumber()
    {
        return _selectNumber;
    }


    //機能 : 選択した曲ファイル名
    //引数 : なし
    //戻り値：なし
    public static string getSelectMusic()
    {
        return _selectMusicFileName;
    }

    //機能 : 選択した曲のcsvファイル名
    //引数 : なし
    //戻り値：なし
    public static string getSelectCSV()
    {
        return _selectCsv;
    }

    //機能 : 選択した曲の表示する名前
    //引数 : なし
    //戻り値：なし
    public static string getSlectMusicName()
    {
        return _selectName;
    }

    //機能 : プレイに必要なデータを保存しているstaticの変数を全てnullにする
    //引数 : なし
    //戻り値：なし
    public static void resetSelectData()
    {
        _selectMusicFileName = null;
        _selectCsv = null;
        _selectName = null;
    }

    //ボタンを有効化・無効化する
    //引数：false(無効化) true(有効化)
    //戻り値：なし
    public void setInterctive(bool isInteractive)
    {
        //各ボタンを引数に応じて有効化・無効化する
        for (int i = 0; i < _button.Length; i++)
        {
            _button[i].interactable = isInteractive;
        }
    }

    // 真ん中のボタンのサイズを大きくしてボタン機能を使えるようにする
    // 引数:
    // 第一引数 : 真ん中のボタンのID
    // 第二引数 : 元真ん中のボタンのID
    // 戻り値 : なし
    public void centerButtonActive(int centerID, int srcID)
    {
        // ボタン機能を使えるようにする
        // 元のボタンは使えないようにする
        _button[srcID].interactable = false;
        _button[centerID].interactable = true;

        // 選択された曲をループ再生する
        _audioLoop.setInfo(_selMusic[centerID].loopStartTime, _selMusic[centerID].loopEndTime, Audio.getInstance().getAudio(centerID));

    }

    //機能 : CSVファイルに書き込んだ譜面データを取得する
    //引数 : なし
    //戻り値 : なし
    private void loadCSVData()
    {

        // ストリームリーダーsrに読み込む
        //csvファイルを読み込む
        TextAsset csvFile = Resources.Load(_filePass) as TextAsset;

        StringReader sr = new StringReader(csvFile.text);

        //要素数に使う変数
        int i = 0;

        //次の文字(データ)がなければ終了
        while (sr.Peek() > -1)
        {
            //行を取り出す
            string line = sr.ReadLine();
            //行データをカンマ区切りで分けて配列に取り出す
            string[] val = line.Split(',');

            //曲情報にIDを割り当て
            _selMusic[i].id = i;

            //csvに保存されている曲の情報を保存

            //曲のファイル名
            _selMusic[i].fileName = "Sound/PlayMusic/" + val[0];

            //曲の譜面ファイル名
            _selMusic[i].csvName = "CSV/" + val[1];
            
            //画面に表示する曲の名前
            _selMusic[i].musicName = val[2];
            
            //選択ボタンに適用する曲のスプライト
            _selMusic[i].musicSprite = Resources.Load("Text/" + val[2], typeof(Sprite)) as Sprite;
            
            //曲
            //ゲームを起動して最初の処理かどうか確認する
            if (Audio.getInstance().confirmAudios() == false)
            {
                _selMusic[i].clip = Resources.Load(_selMusic[i].fileName, typeof(AudioClip)) as AudioClip;
                _audioLoad[i] = _selMusic[i].clip;
            }
            else
            {
                _selMusic[i].clip = Audio.getInstance().getAudio(i);
            }

            //ループの開始時間と終了時間
            _selMusic[i].loopStartTime = float.Parse(val[3]);
            _selMusic[i].loopEndTime = float.Parse(val[4]);

            //次の要素へ
            i++;
        }
        if (Audio.getInstance().confirmAudios() == false)
        {
            Audio.getInstance().setAudio(_audioLoad);
        }
    }
}
