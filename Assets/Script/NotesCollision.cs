﻿//----------------- NotesCollision -----------------
//
//      概要 : ノーツの当たり判定を行う
//      内容 : ノーツの時間とタップ、フリックしたときの種類、時間があっているか
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class NotesCollision : MonoBehaviour
{

    private InputManager _inputManager;     //入力判定
    private NotesCreate _notesCreate;       //ノーツ作成クラス
    private Score _score;                   //スコアを管理するクラス
    private Effect _effect;                 //タップ時にエフェクトを出したい
    private Life _life;                     //体力を管理するクラス
    private Shake _shake;                   //フィーバータイムに突入した際に携帯を振ります
    private Image _stageImage;              //ステージ背景を変更するために使います


    private float[] _notesTime;             //ノーツの時間
    private float _startTime = 0;           //開始時間
    private float _feverTime = 0;           //フィーバータイムに突入する時間
    private float _perfectTime = 0;         //perfectの時間
    private float _greatTime = 0;           //greatの時間
    private float _goodTime = 0;            //goodの時間
    private float _missTime = 0;            //missの時間
    private int[] _notesCate;               //ノーツの種類
    private bool _playFlg = false;          //プレイ中:true プレイしていない:false
    private bool _longFlg = false;          //ロングノーツの判定をしているかどうか
    private bool _feverFlg = false;         //フィーバー中ならtrue
    private List<GameObject> _notes;        //画面上に存在しているノーツ
    private List<GameObject> _longNotes;    //画面上に存在するロングノーツ
    private List<int> _deleteAry;           //削除する要素数をまとめて格納

    //public AudioClip _tapSE;                //タップのSE
    public GameObject _feverBack;           //フィーバータイムに突入した際の人アニメ
    public GameObject _stage;               //背景
    public GameObject _collisionBar;        //判定バー
    public Sprite _feverSprite;             //フィーバー時の背景


    //デバッグ
    private Text _text;
    private int[] _debugMeasure;
    private int[] _debugBeat;
    public bool _debugMode = false;

    

    // Use this for initialization
    void Start()
    {
        // SEのロード
        Audio.getInstance().loadSE((int)Const.AudioSE.SYAN, Const.AudioSEName.SYAN);
        Audio.getInstance().loadSE((int)Const.AudioSE.FLICK, Const.AudioSEName.FLICK);
        Audio.getInstance().loadSE((int)Const.AudioSE.MISS, Const.AudioSEName.MISS);

        _notes = new List<GameObject>();
        _longNotes = new List<GameObject>();
        _deleteAry = new List<int>();

        _inputManager = GameObject.Find("DataBase").GetComponent<InputManager>();
        _effect = GameObject.Find("ParticleCanvas").GetComponent<Effect>();
        _shake = GameObject.Find("DataBase").GetComponent<Shake>();
        _stageImage = _stage.GetComponent<Image>();
        

        if(_debugMode == false)
        {
            _score = GameObject.Find("DataBase").GetComponent<Score>();
            _life = GameObject.Find("DataBase").GetComponent<Life>();
        }
        else
        {
            _text = GameObject.Find("Canvas/Text").GetComponent<Text>();
        }

    }

    // Update is called once per frame
    void Update()
    {
        //ノーツの判定

        #region ゲームプレイ中
        if (_playFlg == true)
        {
            #region 
            if (_feverFlg == false)
            {

                #region ノーツの判定を行う

                int i = 0;

                //タップ判定を行う
                if (_inputManager.clickScreen())
                {
                    for (i = 0; i < _notes.Count; i++)
                    {
                        if (_notes[i] != null && _notesCate[i] == (int)Const.TapCate.TAP)
                        {
                            notesDecision(_notes[i].GetComponent<Notes>().getNotesTime(), getMusicTime(), _perfectTime, i, (int)Const.TapCate.TAP);
                        }
                    }
                }

                //フリック判定を行う
                int flick = _inputManager.flickScreenX();
                //左フリック
                if (flick == (int)Const.TapCate.FLICK_LEFT)
                {
                    for (i = 0; i < _notes.Count; i++)
                    {
                        if (_notes[i] != null && _notesCate[i] == (int)Const.TapCate.FLICK_LEFT)
                        {
                            notesDecision(_notes[i].GetComponent<Notes>().getNotesTime(), getMusicTime(), _perfectTime, i, (int)Const.TapCate.FLICK_LEFT);
                        }
                    }
                }
                //右フリック
                else if (flick == (int)Const.TapCate.FLICK_RIGHT)
                {
                    for (i = 0; i < _notes.Count; i++)
                    {
                        if (_notes[i] != null && _notesCate[i] == (int)Const.TapCate.FLICK_RIGHT)
                        {
                            notesDecision(_notes[i].GetComponent<Notes>().getNotesTime(), getMusicTime(), _perfectTime, i, (int)Const.TapCate.FLICK_RIGHT);
                        }
                    }
                }


                //ロングノーツ判定を行う
                int longTap = _inputManager.longTap();
                //ロングはじめ
                if (longTap == (int)Const.TapCate.LONG_START)
                {
                    //ロングタップの始まり
                    //画面に触れたとき
                    for (i = 0; i < _notes.Count; i++)
                    {
                        if (_notes[i] != null && _notesCate[i] == (int)Const.TapCate.LONG_START)
                        {
                            //ロングノーツ判定開始
                            notesLongDecision(_notes[i].GetComponent<Notes>().getNotesTime(), getMusicTime(), _perfectTime, i, true);
                        }
                    }

                }
                //ロングおわり
                else if (_longFlg == true && longTap == (int)Const.TapCate.LONG_STOP)
                {
                    //ロングタップの終わり
                    //画面から指を離したとき
                    for (i = 0; i < _notes.Count; i++)
                    {
                        if (_notes[i] != null && _notesCate[i] == (int)Const.TapCate.LONG_STOP)
                        {
                            notesLongDecision(_notes[i].GetComponent<Notes>().getNotesTime(), getMusicTime(), _perfectTime, i, false);
                            _longFlg = false;
                        }
                    }
                }


                //通り過ぎたノーツは消去リスト行き
                for (i = 0; i < _notes.Count; i++)
                {

                    if (_notes[i] != null && _notes[i].GetComponent<Notes>().getNotesTime() + _missTime / 2 <= getMusicTime())
                    {
                        // 既に消去候補に入っていればコンテ
                        bool flg = false;
                        foreach (int j in _deleteAry)
                        {
                            if (j == i)
                                flg = true;
                        }
                        if (flg == true)
                            continue;

                        // ミスノーツがロングスタートの場合
                        if (_notesCate[i] == (int)Const.TapCate.LONG_START)
                        {
                            // 
                            if (_longFlg == false)
                            {
                                deleteLongNotes(i);
                            }
                            else
                            {
                                continue;
                            }
                        }

                        // ミスノーツがロングストップの場合
                        if (_notesCate[i] == (int)Const.TapCate.LONG_STOP)
                        {
                            deleteLongNotes(i - 1);
                        }

                        if (_debugMode == false)
                        {
                            _score.addScore((int)Const.ScoreCate.MISS);
                            _life.setLife(-1);
                        }

                        _deleteAry.Add(i);
                    }

                }
                #endregion

                #region フィーバータイム突入
                if (_feverTime <= getMusicTime())
                {
                    // Let's party
                    _feverFlg = true;

                    //コンボを消す
                    _score.setComboEnabled();

                    // ライフを消す
                    _life.lifeEnable();

                    // 判定バーを消す
                    _collisionBar.SetActive(false);

                    // 背景をフィーバー
                    _stageImage.sprite = _feverSprite;

                    // パリピを召喚
                    GameObject back = Instantiate(_feverBack, new Vector3(0, 1.5f, 0), Quaternion.identity);
                    back.GetComponent<SpriteRenderer>().sortingOrder = 1;

                    //バイブレーション
                    Handheld.Vibrate();

                    //シェイク機能をON
                    _shake.setShakeFlg(true);
                }
                #endregion

                //不要なノーツをまとめて削除
                deleteNotes();
            }
            #endregion

        }
        #endregion
    }


    //機能 : プレイするときに呼ぶ初期準備関数
    //引数 : なし
    //戻り値：なし
    public void initFunc()
    {
        _notesCreate = GameObject.Find("DataBase").GetComponent<NotesCreate>();

        //ノーツの時間を格納した配列をコピー
        float[] src = _notesCreate.getNotesTime();
        _notesTime = new float[src.Length];
        Array.Copy(src, _notesTime, src.Length);

        //ノーツ種類を格納した配列をコピー
        int[] srcCate = _notesCreate.getNotesCategory();
        _notesCate = new int[srcCate.Length];
        Array.Copy(srcCate, _notesCate, srcCate.Length);

        //デバッグ
        //ノーツ種類を格納した配列をコピー
        int[] srcMea = _notesCreate.getDebugMeasure();
        _debugMeasure = new int[srcMea.Length];
        Array.Copy(srcMea, _debugMeasure, srcMea.Length);

        int[] srcBeat = _notesCreate.getDebugBeat();
        _debugBeat = new int[srcBeat.Length];
        Array.Copy(srcBeat, _debugBeat, srcBeat.Length);
    }

    public void setPlayFlg(bool flg)
    {
        _playFlg = flg;
    }


    //機能 : スタートした時間を渡す
    //引数 : 曲(ゲーム)開始時間
    //戻り値：なし
    public void setStartTime(float time)
    {
        _startTime = time;
    }

    //機能 : フィーバーの時間をセットする
    //引数 : フィーバー突入時間
    //戻り値：なし
    public void setFeverTime(float time)
    {
        _feverTime = time;
    }

    //機能 : 種類ごとの判定時間をセットする
    //引数 :
    //第1引数 : パーフェクトの時間
    //第2引数 : グレートの時間
    //第3引数 : グッドの時間
    //第4引数 : ミスの時間
    //戻り値：なし
    public void setCollisionsTime(float perfect,float great,float good,float miss)
    {
        _perfectTime = perfect;
        _greatTime = great;
        _goodTime = good;
        _missTime = miss;
    }

    //ノーツの当たり判定(秒単位で判定をする)
    //引数
    //第1引数：判定ラインにくる時間 
    //第2引数：曲の再生時間(再生してから何秒たったのか)
    //第3引数：判定時間(秒)
    //第4引数 : 要素数
    //戻り値：なし
    public void notesDecision(float lineTime, float playTime, float deciTime, int ary)
    {


        //abs(判定ラインにくる時間 - 曲の再生時間) <= 判定時間(秒) / 2
        if (Mathf.Abs(lineTime - playTime) <= _perfectTime / 2)
        {
            _effect.createEffect();
            if (_debugMode == false)
                _score.addScore((int)Const.ScoreCate.PERFECT);
            else
                setDebugText(ary);//デバッグ用
            Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
        else if(Mathf.Abs(lineTime - playTime) <= _greatTime / 2)
        {
            _effect.createEffect();
            if (_debugMode == false)
                _score.addScore((int)Const.ScoreCate.GREAT);
            else
                setDebugText(ary);//デバッグ用
            Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
        else if(Mathf.Abs(lineTime - playTime) <= _goodTime / 2)
        {
            _effect.createEffect();
            if (_debugMode == false)
                _score.addScore((int)Const.ScoreCate.GOOD);
            else
                setDebugText(ary);//デバッグ用
            Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
        else if(Mathf.Abs(lineTime - playTime) <= _missTime / 2)
        {
            _effect.createEffect();
            if (_debugMode == false)
                _score.addScore((int)Const.ScoreCate.BAD);
            else
                setDebugText(ary);//デバッグ用
            Audio.getInstance().playSE((int)Const.AudioSE.MISS);
            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
    }

    //ノーツの当たり判定(秒単位で判定をする)
    //引数
    //第1引数：判定ラインにくる時間 
    //第2引数：曲の再生時間(再生してから何秒たったのか)
    //第3引数：判定時間(秒)
    //第4引数 : 要素数
    //第5引数 : タップのカテゴリ
    //戻り値：なし
    public void notesDecision(float lineTime, float playTime, float deciTime, int ary, int cate)
    {


        //abs(判定ラインにくる時間 - 曲の再生時間) <= 判定時間(秒) / 2
        if (Mathf.Abs(lineTime - playTime) <= _perfectTime / 2)
        {
            _effect.createEffect();
            if (_debugMode == false)
                _score.addScore((int)Const.ScoreCate.PERFECT);
            else
                setDebugText(ary);//デバッグ用

            if (cate == (int)Const.TapCate.TAP)
                Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
            else if (cate == (int)Const.TapCate.FLICK_RIGHT || cate == (int)Const.TapCate.FLICK_LEFT)
                Audio.getInstance().playSE((int)Const.AudioSE.FLICK);

            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
        else if (Mathf.Abs(lineTime - playTime) <= _greatTime / 2)
        {
            _effect.createEffect();
            if (_debugMode == false)
                _score.addScore((int)Const.ScoreCate.GREAT);
            else
                setDebugText(ary);//デバッグ用

            if (cate == (int)Const.TapCate.TAP)
                Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
            else if (cate == (int)Const.TapCate.FLICK_RIGHT || cate == (int)Const.TapCate.FLICK_LEFT)
                Audio.getInstance().playSE((int)Const.AudioSE.FLICK);
            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
        else if (Mathf.Abs(lineTime - playTime) <= _goodTime / 2)
        {
            _effect.createEffect();
            if (_debugMode == false)
                _score.addScore((int)Const.ScoreCate.GOOD);
            else
                setDebugText(ary);//デバッグ用

            if (cate == (int)Const.TapCate.TAP)
                Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
            else if (cate == (int)Const.TapCate.FLICK_RIGHT || cate == (int)Const.TapCate.FLICK_LEFT)
                Audio.getInstance().playSE((int)Const.AudioSE.FLICK);

            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
        else if (Mathf.Abs(lineTime - playTime) <= _missTime / 2)
        {
            _effect.createEffect();
            if (_debugMode == false)
                _score.addScore((int)Const.ScoreCate.BAD);
            else
                setDebugText(ary);//デバッグ用

            
            Audio.getInstance().playSE((int)Const.AudioSE.MISS);
            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
    }

    //ノーツの当たり判定(秒単位で判定をする)
    //引数
    //第1引数：判定ラインにくる時間 
    //第2引数：曲の再生時間(再生してから何秒たったのか)
    //第3引数：判定時間(秒)
    //第4引数 : 要素数
    //第5引数 : ロングノーツの始点か終点か  始点:true,終点:false
    //戻り値：なし
    public void notesLongDecision(float lineTime, float playTime, float deciTime, int ary, bool flg)
    {

        //abs(判定ラインにくる時間 - 曲の再生時間) <= 判定時間(秒) / 2
        if (Mathf.Abs(lineTime - playTime) <= deciTime / 2)
        {
            if (flg == true)
            {
                //スタートタッチ
                _longFlg = true;
                _effect.createEffect();
                if (_debugMode == false)
                    _score.addScore((int)Const.ScoreCate.PERFECT);
                else
                    setDebugText(ary);//デバッグ用
                //GetComponent<AudioSource>().PlayOneShot(_tapSE);
                Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
                _notes[ary].GetComponent<Notes>().setNotesStartFlg(true);
                _longNotes[0].GetComponent<LongNotes>().setStopFlg(true);
            }
            else
            {
                //ロング成功
                _longFlg = false;
                _effect.createEffect();
                if (_debugMode == false)
                    _score.addScore((int)Const.ScoreCate.PERFECT);
                else
                    setDebugText(ary);//デバッグ用
                Audio.getInstance().playSE((int)Const.AudioSE.SYAN);
                //GetComponent<AudioSource>().PlayOneShot(_tapSE);
                //当たり判定を行ったノーツの要素数を消去リストにぶち込む
                Destroy(_longNotes[0].gameObject);
                _longNotes.Remove(_longNotes[0]);
                _deleteAry.Add(ary - 1);
                _deleteAry.Add(ary);
                
            }
        }
        else if (_longFlg == true && flg == false)
        {
            Audio.getInstance().playSE((int)Const.AudioSE.MISS);
            //離しちゃったー
            _longFlg = false;
            if (_debugMode == false)
            {
                _score.addScore((int)Const.ScoreCate.MISS);
                _life.setLife(-1);
            }

            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            Destroy(_longNotes[0].gameObject);
            _longNotes.Remove(_longNotes[0]);
            _deleteAry.Add(ary - 1);
            _deleteAry.Add(ary);
        }
    }

    //ノーツの当たり判定(秒単位で判定をする)
    //引数
    //第1引数：判定ラインにくる時間 
    //第2引数：曲の再生時間(再生してから何秒たったのか)
    //第3引数：判定時間(秒)
    //第4引数 : 要素数
    //戻り値：なし
    public void notesShakeDecision(float lineTime, float playTime, float deciTime, int ary)
    {

        //abs(判定ラインにくる時間 - 曲の再生時間) <= 判定時間(秒) / 2
        if (lineTime - playTime <= 0)
        {
            GameObject back = Instantiate(_feverBack, new Vector3(0, 0, -1), Quaternion.identity);
            back.GetComponent<SpriteRenderer>().sortingOrder = -1;    
            Handheld.Vibrate();
            _shake.setShakeFlg(true);
            //当たり判定を行ったノーツの要素数を消去リストにぶち込む
            _deleteAry.Add(ary);
        }
    }


    //不要なノーツを削除する
    //引数 : なし
    //戻り値：なし
    private void deleteNotes()
    {
        //消去リストにあるノーツを消す
        for (int i = 0; i < _deleteAry.Count; i++)
        {
            if(_notes[_deleteAry[i]] != null)
                Destroy(_notes[_deleteAry[i]].gameObject);
        }

        //消去リストをクリアする
        _deleteAry.Clear();
    }


    //機能 : ロングノーツ一家の削除準備
    //引数 : ロングノーツの始点要素数
    //戻り値：なし
    private void deleteLongNotes(int startAry)
    {
        // 始点と終点を消去リストにぶちこむ
        _deleteAry.Add(startAry);
        _deleteAry.Add(startAry + 1);

        // 長い部分を削除する
        Destroy(_longNotes[0].gameObject);
        _longNotes.Remove(_longNotes[0]);


    }

    //機能 : ノーツオブジェクトを判定リストに追加
    //引数 : ノーツオブジェクト
    //戻り値：なし
    public void setAddNotes(GameObject obj)
    {
        _notes.Add(obj);
    }


    //機能 : ロングノーツオブジェクトをリストに追加
    //引数 : ロングノーツオブジェクト
    //戻り値：なし
    public void setAddLongNotes(GameObject obj)
    {
        _longNotes.Add(obj);
    }

    //機能 : 曲開始からどのくらい経ったのかfloat秒単位で返す
    //          ゲームの開始した時間 - 曲を開始した時間 = 曲開始からの時間(秒)
    //引数 : なし
    //戻り値 : 曲開始からの時間
    private float getMusicTime()
    {
        return Time.time - _startTime;
    }

    //機能 : デバッグ用で判定したノーツの小節と拍をconsoleに表示する
    //引数 : ノーツの要素番号
    //戻り値 : なし
    private void setDebugText(int ary)
    {
        _text.text = "小節 : " + _debugMeasure[ary].ToString() + ", 拍 : " + _debugBeat[ary].ToString();
    }
}
