﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Result : MonoBehaviour {
    public Image _numFont;
    public Image _rankFont;
    private NumberFont _font;
    private Image[] _perfectNum;
    private Image[] _greatNum;
    private Image[] _goodNum;
    private Image[] _badNum;
    private Image[] _missNum;
    private Image[] _scoreNum;
    private Image[] _maxComboNum;
    private string[] _numberPanel;
    private string[] _rankFileName;

	// Use this for initialization
	void Start () {
        _font = GameObject.Find("DataBase").GetComponent<NumberFont>();
        
        _rankFileName = new string[Const.Consts.RANK] { "S", "A", "B", "C" };

        _rankFont.sprite = Resources.Load<Sprite>("Rank/" + _rankFileName[Score.getScoreRankId()]);

        //スコアをリセットする
        _perfectNum = new Image[Const.Consts.SCORE_CATE_DIGIT];
        _greatNum = new Image[Const.Consts.SCORE_CATE_DIGIT];
        _goodNum = new Image[Const.Consts.SCORE_CATE_DIGIT];
        _badNum = new Image[Const.Consts.SCORE_CATE_DIGIT];
        _missNum = new Image[Const.Consts.SCORE_CATE_DIGIT];
        _scoreNum = new Image[Const.Consts.SCORE_DIGITS];
        _maxComboNum = new Image[Const.Consts.COMBO_DIGITS];


        createNumFont(ScoreData.score, _scoreNum,"Score");
        createNumFont(ScoreData.maxComboCnt, _maxComboNum, "MaxCombo");
        createNumFont(ScoreData.perfectCnt, _perfectNum, "Perfect");
        createNumFont(ScoreData.greatCnt, _greatNum, "Great");
        createNumFont(ScoreData.goodCnt, _goodNum, "Good");
        createNumFont(ScoreData.badCnt, _badNum, "Bad");
        createNumFont(ScoreData.missCnt, _missNum, "Miss");

        // スコアをリセット
        ScoreData.resetScore();

	}
	
	// Update is called once per frame
	void Update () {

	}
    
    //桁数を数える
    //引数：桁数を数えたいやつ
    //戻り値：桁数
    private int digitNum(int num)
    {
        int cnt = 1;

        for (int i = num; i >= 10;i/=10)
        {
            cnt++;
        }
        return cnt;
    }

    //スコアスプライトの配置を計算する
    //引数：桁数
    //戻り値：数字の配置
    private float[] locatePos(int digit)
    {
        //桁数分確保
        float[] locate = new float[digit];
        //奇数なら配列の中心の値になる
        int center = digit / 2;
        int width = 32;

        int i = 0;
        int pos = 0;
        //左端の位置をとる
        
        if(digit % 2 == 0)
        {
            //偶数
            pos = width + ((width * 2) * (center - 1));
        }
        else
        {
            //奇数
            pos = width * 2 * center;
        }

        for (i = 0; i < digit;i++ )
        {
            locate[i] = pos;

            pos = pos - width * 2;
        }


        return locate;
    }

    //数字フォントを使いリザルトを表示する
    //引数：桁数　インスタンスしたオブジェクトをいれる配列 最初に設定してある
    //戻り値：なし
    private void createNumFont(int num, Image[] numTex,string parentName)
    {
        int digit = digitNum(num);
        float[] locate = locatePos(digit);
        for (int i = 0; i < digit; i++)
        {
            numTex[i] = Instantiate(_numFont,new Vector2(locate[i],0),Quaternion.identity);
            numTex[i].transform.SetParent(GameObject.Find("Canvas/" + parentName).transform, false);
        }

        _font.showFont(num,numTex);
    }

    
}
