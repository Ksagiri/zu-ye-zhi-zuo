﻿//----------------- LongNotes -----------------
//
//      概要 : ロングノーツの長い部分の管理
//      内容 : 始点と終点の間で伸ばす
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongNotes : MonoBehaviour {

    private float _notesTimeEnd;       //このノーツが枠につくタイミング
    private float _notesTimeStart;

    private float _musicStartTime = 0;   //開始時間
    private bool _stopFlg = false;

    public float _speed = 0.4f;    //ノーツの速さ
    public int _lineLen = 12;      //レーンの長さ

	// Use this for initialization
	void Start () 
    {
		
	}
	
	// Update is called once per frame
	void Update () 
    {
        move(1, _notesTimeStart,_notesTimeEnd, getMusicTime());
	}

    //ノーツの移動処理
    //引数
    //第1引数 終点から始点への方向 
    //第2引数 判定のラインにくる時間開始 
    //第3引数 判定のラインにくる時間終了 
    //第4引数 曲の再生時間(再生してから何秒たったのか) 
    //戻り値：float型　計算結果が戻り値になる
    public void move(int dir, float lineTimeS,float lineTimeE, float playTime)
    {
        //開始ノーツの位置を求める
        float endPos = Const.Consts.COLLISION_POSX + dir * ((lineTimeS - playTime) * _lineLen * _speed);
        if(_stopFlg == true && endPos < Const.Consts.COLLISION_POSX)
            endPos = Const.Consts.COLLISION_POSX;
        this.transform.position = new Vector3(endPos,0,1);

        //開始ノーツと終了ノーツの間で伸ばす
        float endScale = Const.Consts.COLLISION_POSX + dir * ((lineTimeE - playTime) * _lineLen * _speed);
        this.transform.localScale = new Vector3((endScale - endPos), 1, 1);

    }


    //機能 : ノーツ作成されるときに必要な時間データを渡す
    //引数
    //第1引数 : 開始のロングノーツが判定ラインにくる時間
    //第1引数 : 離すロングノーツが判定ラインにくる時間
    //第2引数 : 楽曲が始まる時間(譜面開始の時間)
    //戻り値 : 曲開始からの時間
    public void setCreate(float startTime, float endTime, float musicStartTime)
    {
        _notesTimeStart = startTime;
        _notesTimeEnd = endTime;
        _musicStartTime = musicStartTime;

    }

    //機能 : 曲開始からどのくらい経ったのかfloat秒単位で返す
    //          ゲームの開始した時間 - 曲を開始した時間 = 曲開始からの時間(秒)
    //引数 : なし
    //戻り値 : 曲開始からの時間
    private float getMusicTime()
    {
        return Time.time - _musicStartTime;
    }

    //機能 : 長いノーツを判定場所にとどめるかどうかのセッター
    //引数 : true -> とまる  false -> 流れていく
    //戻り値：なし
    public void setStopFlg(bool flg)
    {
        _stopFlg = flg;
    }
}
