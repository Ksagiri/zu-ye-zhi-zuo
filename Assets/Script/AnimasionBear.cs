﻿//----------------- Animation -----------------
//
//      概要 : プレイシーンのアニメーションの管理
//      内容 : くまのアニメと背景画像を変更する
//
//      作成者 : 工藤　祐平(修正)
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class AnimasionBear : MonoBehaviour {
    private Image _stageImage;      // ステージのImageコンポーネント

    public GameObject _bear;        // くま
    public GameObject _stage;       // ステージ
    


	// Use this for initialization
	void Start () {

        //ステージのコンポーネント
        _stageImage = _stage.GetComponent<Image>();

        //アニメーションコントローラーのパス
        string pass = "Animasion/" + SelectClothesButton.getSelectAnimName() + "Ctrl";

        //くまとステージが対応していたらくまのアニメーションを特殊なものにする
        if (SelectClothesButton.getClothesId() == SelectStageButton.getStageId() + 1)
        {
            //サイズを調整
            _bear.transform.localScale = new Vector3(0.6f, 0.6f, 1.0f);
            
            //特殊くまの追加パス名
            pass += "_SP";
        }

        //アニメーション
        _bear.GetComponent<Animator>().runtimeAnimatorController = (RuntimeAnimatorController)RuntimeAnimatorController.Instantiate(Resources.Load(pass));


        //スプライト
        _stageImage.sprite = Resources.Load("Stage/" + SelectStageButton.getSpritePass(), typeof(Sprite)) as Sprite;
	}
	
	// Update is called once per frame
	void Update () 
    {
		
	}

    
}
