﻿//----------------- Const -----------------
//
//      概要 : 定数をまとめている
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Const
{
    // 画面タップの種類
    enum TapCate
    {
        TAP,
        FLICK_LEFT,
        FLICK_RIGHT,
        LONG_START,
        LONG_STOP,
        SHAKE
    }

    // スコアの種類
    enum ScoreCate
    {
        PERFECT,
        GREAT,
        GOOD,
        BAD,
        MISS
    }

    //SEの種類
    enum AudioSE
    {
        SYAN,
        FLICK,
        MISS,
        OK,
        SELECT,
        CANCEL,
        END_VOICE
    }

    //SEの名前
    struct AudioSEName
    {
        public const string SYAN = "syan_se";           // 曲タップ
        public const string FLICK = "flick_se";         // 曲フリック
        public const string MISS = "miss_se";           // 曲ミス
        public const string OK = "ok_se";               // 決定
        public const string SELECT = "select_se";       // 選択
        public const string CANCEL = "cancel_se";       // キャンセル
        public const string END_VOICE = "endVoice_se";  // 曲終了時の歓声
    }

	
    // 加算するスコア
    struct AddScore
    {
        public const int PAFECT = 20;
        public const int GREAT = 15;
        public const int GOOD = 10;
        public const int BAD = 1;
        public const int MISS = 0;
        public const int SHAKE = 30;
    }

    // 様々な合計数
    struct Consts
    {
        public const int MUSIC = 5;
        public const int SE = 7;
        public const int CLOTHES = 7;
        public const int STAGE = 6;
        public const int RANK = 4;
        public const int COLLISION_POSX = -2;
        public const int SCORE_DIGITS = 5;
        public const int COMBO_DIGITS = 3;
        public const int SCORE_CATE_DIGIT = 3;

    }
}
