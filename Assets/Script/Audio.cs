﻿//----------------- Audio -----------------
//
//      概要 : プレイするのに必要なAudioを保持する
//      内容 : ロードしたAudioClipを静的に保持する
//              staticを使用、シングルトン
//
//      作成者 : 工藤　祐平
//
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Audio
{
    private GameObject _object = null;          // SEのチャンエル作成のため

    private static Audio _singleton = null;     // 実体

    private AudioClip[] _audios;                // オーディオを保存する配列

    private AudioSE[] _audioSE;                 // SE情報を保有するクラス

    private AudioSource[] _seChannel;           // SEのチャンネル



    //機能 : クラスインスタンスを返す(static)
    //引数 : なし
    //戻り値 : なし
    public static Audio getInstance()
    {
        // 存在していなければ実体を作成する
        if (_singleton == null)
        {
            _singleton = new Audio();
        }

        return _singleton;
    }

    //機能 : Audioクラスのコンストラクタ
    //引数 : なし
    //戻り値 : なし
    public Audio()
    {
        //Audioの静的領域確保
        _audios = new AudioClip[Const.Consts.MUSIC];

        _audioSE = new AudioSE[Const.Consts.SE];

        //SEの静的領域確保
        _seChannel = new AudioSource[Const.Consts.SE];
    }

    //SEのデータクラス
    //追加の可能性があるのでクラス化した
    private class AudioSE
    {
        private string _pass = "Sound/SE/";
        public string _name;
        public AudioClip _clip;
 
        //コンストラクタ
        public AudioSE(string name)
        {
            _name = name;
            _clip = Resources.Load(_pass + _name, typeof(AudioClip)) as AudioClip;
        }
    }

    //機能 : 必要なSEをロードする
    //          オブジェクトが存在していない場合はチャンネルを作成してくれる
    //引数 : なし
    //戻り値 : なし
    public void loadSE(int id, string name)
    {
        //オブジェクトがなかったら作成する
        if(_object == null)
        {
            _object = new GameObject("SEChannel");
            
            //チャンネル作成
            for(int i = 0; i < Const.Consts.SE; i++)
            {
                _seChannel[i] = _object.AddComponent<AudioSource>();
            }
        }

        //seが存在しなかったらロードする
        if(_audioSE[id] == null)
        {
            _audioSE[id] = new AudioSE(name);
        }
        
        
    }

    //機能 : SEを鳴らす
    //引数 : なし
    //戻り値 : なし
    public void playSE(int id)
    {
        //Debug.Log("SE鳴ったよ : " + id + ", " + _audioSE[id]._clip);
        _seChannel[id].PlayOneShot(_audioSE[id]._clip);
    }

    //機能 : ゲームプレイで使うAudioClipの配列を受け取り配列にコピーする
    //引数 : なし
    //戻り値 : なし
    public void setAudio(AudioClip[] audios)
    {
        //配列をコピー
        Array.Copy(audios, _audios, audios.Length);
    }

    //機能 : 配列の要素数(ID)を渡すと対応した音楽を返す
    //引数 : 曲ID
    //戻り値 : なし
    public AudioClip getAudio(int ary)
    {
        return _audios[ary];
    }

    public bool getSEPlaying(int id)
    {
        return _seChannel[id].isPlaying;
    }

    //機能 : 配列に音楽が格納されているか確認する
    //          格納されている : true    格納されていない : false
    //引数 : なし
    //戻り値 : bool
    public bool confirmAudios()
    {
        //何もロードされていない状態
        if(_audios[0] == null)
            return false;

        return true;
    }
}
