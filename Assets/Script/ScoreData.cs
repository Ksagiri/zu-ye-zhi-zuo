﻿//----------------- ScoreDataクラス -----------------
//
//      概要 : スコアのデータを管理するクラス
//      内容 : スコアのデータをどこでも参照できるように作った
//
//      作成者 : 今野咲霧
//
//
static class ScoreData{
    public static int score = 0;
    public static int maxComboCnt = 0;
    public static int perfectCnt = 0;
    public static int greatCnt = 0;
    public static int goodCnt = 0;
    public static int badCnt = 0;
    public static int missCnt = 0;
    public static int shakeCnt = 0;

    //スコアをリセットする
    public static void resetScore()
    {
        score = 0;
        maxComboCnt = 0;
        perfectCnt = 0;
        greatCnt = 0;
        goodCnt = 0;
        badCnt = 0;
        missCnt = 0;
        shakeCnt = 0;
    }

}
